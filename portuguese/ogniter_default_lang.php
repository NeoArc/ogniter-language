<?php

$lang['og_site_title'] = "Ogniter - Banco de dados para Ogame";
$lang['og_logo_text'] = "ODB";
$lang['og_domain'] = "Domínio";
$lang['og_domains'] = "Domínios";
$lang['og_server'] = "Universo";
$lang['og_servers'] = "Universos";
$lang['og_online_users'] = "On-line";

$lang['og_description'] = 'Banco de dados livre para universos OGame. Galáxia, Posição. Ferramentas ogame';

$lang['og_pick_a_domain'] = "Escolha um domínio";
$lang['og_choose_a_server'] = "Selecione um universo";
$lang['og_latest_servers'] = "Universos últimos";
$lang['screenshots'] = 'Screenshots';

$lang['og_home_title'] = 'Ogniter, <small>banco de dados para Ogame</small>';
$lang['og_home_description'] = 'Informações dos jogadores, alianças e planetas.';
$lang['og_home_description_2'] = 'É a melhor opção se você não pode criar e / ou manter uma Galaxytool.';
$lang['og_home_how_to_start'] = 'Para começar, <strong>selecione</strong> um domínio, e depois <strong>olhar</strong> o seu universo preferido da lista.';
$lang['og_home_comeback'] = 'Estamos em contínua mudança. , Volte sempre!';

$lang['og_home_domain_list'] = 'Lista dos domínios';
$lang['og_number_of_servers'] = '# de universos registrados';
$lang['og_news'] = 'Notícias';

$lang['og_terms_of_use'] = 'Termos de Utilização';
$lang['og_privacy_policy'] = 'Pol&iacute;ticas de privacidad';
$lang['og_copyright'] = 'Política de Privacidade';
$lang['og_developed_by'] = 'Ogniter, criado por';
$lang['og_game_created_by'] = 'Ogame é um jogo de navegador, multi-usuário, criado por Gameforge';

$lang['og_home'] = 'Página Principal';

$lang['top_n_players'] = 'Top %n% Jogadores';
$lang['top_n_alliances'] = 'Top %n% Alianças';

$lang['ogame_top_n_players'] = 'Ogame %n% melhores jogadores';
$lang['ogame_top_n_alliances'] = 'Ogame %n% melhores alianças';
$lang['uni_position'] = 'Posição no universo';

//server list
$lang['og_server_list'] = 'Lista dos universos';
$lang['og_server_list_from'] = 'Lista de universos no domínio %s%';
$lang['og_acs'] = 'SAC';
$lang['og_speed'] = 'Velocidade';
$lang['og_def_to_debris'] = 'Defesa a escombros';
$lang['og_num_players'] = '# jogadores';
$lang['og_num_alliances'] = '# alianças';
$lang['og_max_score'] = 'Pontuação máxima';
$lang['og_enabled'] = 'Ativado';
$lang['og_disabled'] = 'Desabilitado';

//servers module
$lang['og_server_info'] = 'Detalhes do universo';
$lang['og_search'] = 'Pesquisar';
$lang['og_player_statuses'] = 'Status de jogadores';
$lang['og_galaxy'] = 'Galáxia';
$lang['og_ranking'] = 'Posição';

//galaxy
$lang['og_galaxy_view'] = "Visão da galáxia";
$lang['og_weekly_increment'] = 'Aumento<br />semanal';
$lang['og_monthly_increment'] = 'Aumento<br />mensal';
$lang['og_universe'] = "Universo";
$lang['last_update'] = 'Última actualização';
$lang['next_update'] = 'Próxima Atualização (aprox)';
$lang['og_system'] = 'Sistema';
$lang['og_location'] = 'Localização';
$lang['og_planet'] = 'Planeta';
$lang['og_moon'] = 'Lua';
$lang['og_player_status'] = 'Jogador (estado)';
$lang['og_position'] = 'Posição';
$lang['og_alliance'] = 'Aliança';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = 'Há %t%';
//Events in the future (aproximation)
$lang['in_n_time'] = 'Em %t% (aprox)';

$lang['second'] = 'segundo';
$lang['seconds'] = 'segundos';
$lang['minute'] = 'minuto';
$lang['minutes'] = 'minutos';
$lang['hour'] = 'hora';
$lang['hours'] = 'horas';
$lang['day'] = 'día';
$lang['days'] = 'días';
$lang['week'] = 'semana';
$lang['weeks'] = 'semanas';
$lang['month'] = 'mês';
$lang['months'] = 'meses';
$lang['year'] = 'ano';
$lang['years'] = 'anos';

//server detail
$lang['og_name'] = 'Nome';
$lang['og_language'] = 'Linguagem';
$lang['og_timezone'] = 'Região';
$lang['og_ogame_version'] = 'Versão Ogame';
$lang['og_galaxies'] = 'Galáxias';
$lang['og_systems'] = 'Sistemas';
$lang['og_rapid_fire'] = 'Rapid Fire';
$lang['og_debris_factor'] = 'Fator de detritos';
$lang['og_repair_factor'] = 'Defesa reparação';
$lang['og_newbie_protection_limit'] = 'Nova proteção';
$lang['og_newbie_protection_high'] = 'Proteção frotas';
$lang['og_other_info'] = 'Outros dados';
$lang['og_points_limit'] = 'Até %d pontos';

//alliance
$lang['og_homepage'] = 'Homepage';
$lang['og_updated_on'] = 'Atualizado';
$lang['og_score'] = 'Pontos';
$lang['statistics'] = 'Estatística';
$lang['view'] = 'Ver';

$lang['og_total'] = 'Total';
$lang['og_economy'] = 'Economia';
$lang['og_research'] = 'Investigação';
$lang['og_mil_points'] = 'P. Militares';
$lang['og_lost_mil_points'] = 'P. Ml. perderam';
$lang['og_built_mil_points'] = 'P. Ml. construídos';
$lang['og_destroyed_mil_points'] = 'P. Ml. destruídos';
$lang['og_honor'] = 'Honra';
$lang['og_members'] = 'Membros';
$lang['og_alliance_registration'] = 'Registro';
$lang['og_open'] = 'Aberto';
$lang['og_closed'] = 'Fechado';

//player
$lang['og_player'] = 'Jogador';
$lang['og_num_ships'] = '# Navios';
$lang['og_known_planets'] = 'Planetas conhecidos';
$lang['og_type'] = 'Tipo';
$lang['og_size'] = 'Tamanho';

//ranking
$lang['og_results_by_alliance'] = 'Resultados por Aliança';
$lang['og_results_by_player'] = 'Resultados por Jogador';
$lang['og_alliance_tag'] = 'Etiqueta';
$lang['og_average_score_per_player'] = 'Médio por jogador';

//search
$lang['og_search_by'] = 'Pesquisa por';
$lang['og_alliance_tag_long'] = 'Etiqueta do aliança';
$lang['og_text'] = 'Texto';
$lang['og_send'] = 'Enviar';
$lang['og_please_do_a_db_search'] = 'Por favor insira uma pesquisa';
$lang['og_search_results_by'] = 'Resultados da busca por';

//status
$lang['og_inactive'] = 'Inativo';
$lang['og_inactive_30'] = 'Inativo (30 dias)';
$lang['og_v_mode'] = 'Férias';
$lang['og_suspended'] = 'Suspenso';
$lang['og_outlaw'] = 'Bandido';

//statistics
$lang['by_week'] = 'Semanal';
$lang['by_month'] = 'Por mês';
$lang['by_year'] = 'Por ano';
$lang['all'] = 'Todo';

//Compare statistics
$lang['og_comparison'] = 'Comparar';
$lang['compare_statistics'] = 'Comparar a evolução de jogadores e alianças';
$lang['search_players'] = 'Pesquisa do jugadores';
$lang['search_alliances'] = 'Pesquisa do alianças';

//Polls
$lang['vote'] = 'Votar';
$lang['view_results'] = 'Veja o resultados';
$lang['poll_results'] = 'Resultados da votação';
$lang['poll_vote_ok'] = 'Seu voto foi registrado com sucesso.';
$lang['poll_vote_error'] = 'Houve um problema no processamento de seu voto';

//Colonize
$lang['og_colonize'] = 'Colonizar';

//Flight times
$lang['time_calc'] = 'Calcule o tempo de vôo';
$lang['og_flight_times'] = 'Horas de voo';
$lang['start'] = 'Início';
$lang['destination'] = 'Destino';

$lang['og_updating'] = 'Atualizando ...';
$lang['og_wait_a_few_seconds'] = 'Aguarde alguns segundos ... E, em seguida,<a href="%s%">tente novamente</a>';

$lang['flight_time_calculator'] = 'Calcule o tempo de vôo';
$lang['required_field'] = 'Você é obrigado a digitar um valor';
$lang['integer_n_digits'] = 'O valor deve ser um número inteiro até %s% dígitos';
$lang['integer_only'] = 'Solo números inteiros';
$lang['motors'] = 'Motores';
$lang['start_time'] = 'Hora de início';
$lang['fleet_ships'] = 'Navios pertencentes à frota';
$lang['uni_speed'] = 'Velocidade do universo';
$lang['fleet_speed'] = 'Velocidade frota';
$lang['calc_times'] = 'Calcular os tempos';

$lang['result_time'] = 'O tempo de vôo';
$lang['begin_hour'] = 'Hora de início';
$lang['arriving_time'] = 'Horas de chegada';
$lang['end_hour'] = 'Horas de retorno';

$lang['invalidDateTime'] = 'Você deve digitar uma data válida';
$lang['mustAddAShip'] = 'Você deve digitar pelo menos um navio';
$lang['tools'] = 'Ferramentas';

//Resources & Technologies
$lang["small_cargo"] = 'Cargueiro Pequeno';
$lang["large_cargo"] = 'Cargueiro Grande';
$lang["light_fighter"] = 'Caça Ligeiro';
$lang["heavy_fighter"] = 'Caça Pesado';
$lang["cruiser"] = 'Cruzador';
$lang["battle_ship"] = 'Nave de Batalha';
$lang["colony_ship"] = 'Nave Colonizadora';
$lang["recycler"] = 'Reciclador';
$lang["esp_probe"] = 'Sonda de Espionagem';
$lang["bomber_ship"] = 'Bombardeiro';
$lang["solar_sat"] = 'Sat&eacute;lite solar.';
$lang["destroyer"] = 'Destruidor';
$lang["death_star"] = 'Estrela da Morte';
$lang["battle_cruiser"] = 'Interceptador';
$lang["rocket_launcher"] = 'Lançador de Mísseis';
$lang["light_laser"] = 'Laser Ligeiro';
$lang["heavy_laser"] = 'Laser Pesado';
$lang["gauss_cannon"] = 'Canhão de Gauss';
$lang["ion_cannon"] = 'Canhão de Íons';
$lang["plasma_turret"] = 'Canhão de Plasma';
$lang["small_shield_dome"] = 'Pequeno Escudo Planetário';
$lang["large_shield_dome"] = 'Grande Escudo Planetário';

$lang["military_tech"] = 'Tecnologia de Armas';
$lang["defense_tech"] = 'Tecnologia de Escudo';
$lang["hull_tech"] = 'Tecnologia de Blindagem';

$lang["combustion_drive_tech"] = 'Motor de Combustão';
$lang["impulse_drive_tech"] = 'Motor de Impulsão';
$lang["hyperspace_drive_tech"] = 'Motor Propulsor de Hiperespaço';


$lang['theme'] = 'Estilo';
$lang['report_suggestions'] = 'Envie suas sugestões e comentários para <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Universos pertencentes a: ';
$lang['description_domain_module'] = 'Lista universos Ogame: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Ogame ferramentas, as horas de voo.';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, %server% estatísticas em %domain%';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Visão da galáxia [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Visão da galáxia de %server% em %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Procurar jogador, planetas e Alianças (%domain%)';
$lang['description_server_search'] = 'Ogame, pesquisa de dados em %server% - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Ranking (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Rankings em %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. Dados da Aliança %s% em %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, informações sobre os jogadores e da posição da aliança %s% em %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. Dados do jogador %s% em %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, informações sobre os planetas e posição do jogador %s% em %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Comparar jogadores ou alianças. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Comparando jogadores e alianças, %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Gráficos estatísticos, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, informação estatística, %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Top %n% Jogadores';
$lang['description_top_n_players'] = 'Ogniter. Lista de jogadores no top  %n%';

$lang['title_top_n_alliances'] = 'Ogniter. Top %n% Alianças';
$lang['description_top_n_alliances'] = 'Ogniter. Lista de alianças no top %n%';

$lang['pls_donate'] = 'Ogniter é um portal web livre cujos mensal custos são mais elevados do que a média de um servidor web. <br />
             Eu abrir esta seção para usuários interessados ​​em colaborar com a manutenção e desenvolvimento do ogniter.org. Inclua seu nome e E mail!';

$lang['share'] = 'Compartilhar';
$lang['planets'] = 'Planetas';
$lang['moons'] = 'Luas';

$lang['discussions'] = 'Tópicos de discussão';

$lang['find_free_slots'] = 'Encontrar posições livres';
$lang['occupied_planets'] = 'Planetas ocupados';
$lang['find_planets'] = 'Localizar planetas';
$lang['caption'] = 'Descrição';
$lang['free_slots_range'] = 'De %d a %d planetas ocupados';
$lang['n_occupied_planets'] = '%d planetas ocupados';

$lang['new'] = 'Novo';
$lang['alliance_planets'] = 'Planetas Aliança';
$lang['members_statistics'] = 'Members statistics';

$lang['description_server_track_alliance'] = 'Pesquisa planeta do %s% aliança, %server% - %domain%';
$lang['description_server_track_slots'] = 'Busca de posições livres no %server% - %domain%';
$lang['not_found_try_again'] = 'Não foi encontrado. Por favor, tente novamente amanhã.';

$lang['universe_limits'] = 'Limites do universo';
$lang['category'] = 'Categoria';
$lang['thread'] = 'Assunto';

$lang['galaxy_tools'] = 'Ferramentas de galáxias';
$lang['planet_search_by_status'] = 'Pesquisa planetas por estado jogador';
$lang['player_status'] = "Status de jogador";
$lang['by_player_status'] = "Procurar por status do jogador";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Pesquisas';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

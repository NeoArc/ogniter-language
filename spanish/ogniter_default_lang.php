<?php

$lang['og_site_title'] = "Ogniter - Base de datos de Ogame";
$lang['og_logo_text'] = "ODB";
$lang['og_domain'] = "Dominio";
$lang['og_domains'] = "Dominios";
$lang['og_server'] = "Universo";
$lang['og_servers'] = "Universos";
$lang['og_online_users'] = "Usuarios conectados";

$lang['og_description'] = 'Base de datos gratuita de los universos de Ogame. Visi&oacute;n de galaxia, estad&iacute;sticas y herramientas diversas';

$lang['og_pick_a_domain'] = "Elige un dominio";
$lang['og_choose_a_server'] = "Selecciona un universo";
$lang['og_latest_servers'] = "&Uacute;ltimos Universos";
$lang['screenshots'] = 'Capturas de pantalla';

$lang['og_home_title'] = 'Ogniter, <small>base de datos de Ogame</small>';
$lang['og_home_description'] = 'Información de jugadores, alianzas y planetas del juego de navegador Ogame.';
$lang['og_home_description_2'] = 'Es la mejor opción si no puedes crear y/o mantener un Galaxytool.';
$lang['og_home_how_to_start'] = 'Para comenzar, <strong>selecciona</strong> un dominio, y <strong>busca</strong> tu universo preferido en la lista de universos';
$lang['og_home_comeback'] = 'Estamos en modificaciones continuas. Regresa a menudo para ver los cambios!';

$lang['og_home_domain_list'] = 'Listado de dominios';
$lang['og_number_of_servers'] = '# de universos disponibles';
$lang['og_news'] = 'Novedades';

$lang['og_terms_of_use'] = 'T&eacute;rminos de uso';
$lang['og_privacy_policy'] = 'Pol&iacute;ticas de privacidad';
$lang['og_copyright'] = 'Derechos reservados';
$lang['og_developed_by'] = 'Ogniter, desarrollado por';
$lang['og_game_created_by'] = 'Ogame es un juego de navegador, multi-usuario, creado por Gameforge';

$lang['og_home'] = 'Inicio';

$lang['top_n_players'] = 'Jugadores top %n%';
$lang['top_n_alliances'] = 'Alianzas top %n%';

$lang['ogame_top_n_players'] = 'Jugadores top %n% mundial';
$lang['ogame_top_n_alliances'] = 'Alianzas top %n% mundial';
$lang['uni_position'] = 'Posici&oacute;n en el uni';

//server list
$lang['og_server_list'] = 'Listado de universos';
$lang['og_server_list_from'] = 'Listado de universos del dominio %s%';
$lang['og_acs'] = 'SAC';
$lang['og_speed'] = 'Velocidad';
$lang['og_def_to_debris'] = 'Defensas a escombros';
$lang['og_num_players'] = '# jugadores';
$lang['og_num_alliances'] = '# alianzas';
$lang['og_max_score'] = 'Puntaje m&aacute;ximo';
$lang['og_enabled'] = 'Activado';
$lang['og_disabled'] = 'Desactivado';

//servers module
$lang['og_server_info'] = 'Datos del servidor';
$lang['og_search'] = 'Buscar';
$lang['og_player_statuses'] = 'Estado de jugadores';
$lang['og_galaxy'] = 'Galaxia';
$lang['og_ranking'] = 'Ranking';

//galaxy
$lang['og_galaxy_view'] = "Visi&oacute;n de la galaxia";
$lang['og_weekly_increment'] = 'Incremento <br />semanal';
$lang['og_monthly_increment'] = 'Incremento <br />mensual';
$lang['og_universe'] = "Universo";
$lang['last_update'] = 'Ultima actualizaci&oacute;n';
$lang['next_update'] = 'Pr&oacute;xima actualizaci&oacute;n (aprox)';
$lang['og_system'] = 'Sistema';
$lang['og_location'] = 'Ubicaci&oacute;n';
$lang['og_planet'] = 'Planeta';
$lang['og_moon'] = 'Luna';
$lang['og_player_status'] = 'Jugador (estado)';
$lang['og_position'] = 'Posici&oacute;n';
$lang['og_alliance'] = 'Alianza';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = 'Hace %t%';
//Events in the future (aproximation)
$lang['in_n_time'] = 'En %t% (aprox)';

$lang['second'] = 'segundo';
$lang['seconds'] = 'segundos';
$lang['minute'] = 'minuto';
$lang['minutes'] = 'minutos';
$lang['hour'] = 'hora';
$lang['hours'] = 'horas';
$lang['day'] = 'día';
$lang['days'] = 'días';
$lang['week'] = 'semana';
$lang['weeks'] = 'semanas';
$lang['month'] = 'mes';
$lang['months'] = 'meses';
$lang['year'] = 'año';
$lang['years'] = 'años';

//server detail
$lang['og_name'] = 'Nombre';
$lang['og_language'] = 'Lenguaje';
$lang['og_timezone'] = 'Zona horaria';
$lang['og_ogame_version'] = 'Versi&oacute;n de Ogame';
$lang['og_galaxies'] = 'Galaxias';
$lang['og_systems'] = 'Sistemas';
$lang['og_rapid_fire'] = 'Fuego r&aacute;pido';
$lang['og_debris_factor'] = 'Factor de escombros';
$lang['og_repair_factor'] = 'Reparaci&oacute;n de defensas';
$lang['og_newbie_protection_limit'] = 'Protecci&oacute;n a novatos';
$lang['og_newbie_protection_high'] = 'Protecci&oacute;n a flotas';
$lang['og_other_info'] = 'Otros datos';
$lang['og_points_limit'] = 'Hasta %d puntos';

//alliance
$lang['og_homepage'] = 'P&aacute;gina inicial';
$lang['og_updated_on'] = 'Actualizado';
$lang['og_score'] = 'Puntos';
$lang['statistics'] = 'Estad&iacute;sticas';
$lang['view'] = 'Ver';

$lang['og_total'] = 'Total';
$lang['og_economy'] = 'Econom&iacute;a';
$lang['og_research'] = 'Investigaci&oacute;n';
$lang['og_mil_points'] = 'Pt. Militares';
$lang['og_lost_mil_points'] = 'P. Ml. Perdidos';
$lang['og_built_mil_points'] = 'P. Ml. Construidos';
$lang['og_destroyed_mil_points'] = 'P. Ml. Destruidos';
$lang['og_honor'] = 'Honor';
$lang['og_members'] = 'Miembros';
$lang['og_alliance_registration'] = 'Inscripci&oacute;n';
$lang['og_open'] = 'Abierta';
$lang['og_closed'] = 'Cerrada';

//player
$lang['og_player'] = 'Jugador';
$lang['og_num_ships'] = '# Naves';
$lang['og_known_planets'] = 'Planetas conocidos';
$lang['og_type'] = 'Tipo';
$lang['og_size'] = 'Tama&ntilde;o';

//ranking
$lang['og_results_by_alliance'] = 'Resultados por Alianza';
$lang['og_results_by_player'] = 'Resultados por Jugador';
$lang['og_alliance_tag'] = 'Etiqueta';
$lang['og_average_score_per_player'] = 'Promedio x jugador';

//search

$lang['og_search_by'] = 'Buscar por';
$lang['og_alliance_tag_long'] = 'Etiqueta de alianza';
$lang['og_text'] = 'Texto';
$lang['og_send'] = 'Enviar';
$lang['og_please_do_a_db_search'] = 'Por favor, ingresa una busqueda';
$lang['og_search_results_by'] = 'Resultados de busqueda por';

//status
$lang['og_inactive'] = 'Inactivo';
$lang['og_inactive_30'] = 'Inactivo (30 dias)';
$lang['og_v_mode'] = 'Vacaciones';
$lang['og_suspended'] = 'Suspendido';
$lang['og_outlaw'] = 'Proscrito';

//statistics
$lang['by_week'] = 'Por semana';
$lang['by_month'] = 'Por mes';
$lang['by_year'] = 'Por a&ntilde;o';
$lang['all'] = 'Todo';

//Compare statistics
$lang['og_comparison'] = 'Comparar';
$lang['compare_statistics'] = 'Comparar evoluci&oacute;n de jugadores y alianzas';
$lang['search_players'] = 'Buscar jugadores';
$lang['search_alliances'] = 'Buscar alianzas';

//Polls
$lang['vote'] = 'Votar';
$lang['view_results'] = 'Ver resultados';
$lang['poll_results'] = 'Resultados de la encuesta';
$lang['poll_vote_ok'] = 'Se ha registrado tu voto satisfactoriamente.';
$lang['poll_vote_error'] = 'Hubo un problema al procesar tu voto';

//Colonize
$lang['og_colonize'] = 'Colonizar';

//Flight times
$lang['time_calc'] = 'Calcular tiempos de vuelo';
$lang['og_flight_times'] = 'Tiempos de vuelo';
$lang['start'] = 'Inicio';
$lang['destination'] = 'Destino';

$lang['og_updating'] = 'Actualizando...';
$lang['og_wait_a_few_seconds'] = 'Espera unos segundos... , y luego <a href="%s%">inténtalo nuevamente</a>';

$lang['flight_time_calculator'] = 'Calcular tiempos de vuelo';
$lang['required_field'] = 'Es requerido llenar un valor';
$lang['integer_n_digits'] = 'El valor debe ser un numero entero de hasta %s% cifras';
$lang['integer_only'] = 'Solo valores enteros';
$lang['motors'] = 'Motores';
$lang['start_time'] = 'Tiempo de inicio';
$lang['fleet_ships'] = 'Naves pertenecientes a la flota';
$lang['uni_speed'] = 'Velocidad del universo';
$lang['fleet_speed'] = 'Velocidad de las naves';
$lang['calc_times'] = 'Calcular tiempos';

$lang['result_time'] = 'Tiempo de vuelo resultante';
$lang['begin_hour'] = 'Hora de inicio';
$lang['arriving_time'] = 'Hora de llegada';
$lang['end_hour'] = 'Hora de retorno';

$lang['invalidDateTime'] = 'Debes ingresar una fecha v&aacute;lida';
$lang['mustAddAShip'] = 'Debes ingresar al menos una nave';
$lang['tools'] = 'Herramientas';

//Resources & Technologies
$lang["small_cargo"] = 'Nave peque&ntilde;a de carga';
$lang["large_cargo"] = 'Nave grande de carga';
$lang["light_fighter"] = 'Cazador ligero';
$lang["heavy_fighter"] = 'Cazador pesado';
$lang["cruiser"] = 'Crucero';
$lang["battle_ship"] = 'Nave de batalla';
$lang["colony_ship"] = 'Colonizador';
$lang["recycler"] = 'Reciclador';
$lang["esp_probe"] = 'Sonda de espionaje';
$lang["bomber_ship"] = 'Bombardero';
$lang["solar_sat"] = 'Sat&eacute;lite solar.';
$lang["destroyer"] = 'Destructor';
$lang["death_star"] = 'Estrella de la muerte';
$lang["battle_cruiser"] = 'Acorazado';
$lang["rocket_launcher"] = 'Lanzamisiles';
$lang["light_laser"] = 'L&aacute;ser peque&ntilde;o';
$lang["heavy_laser"] = 'L&aacute;ser grande';
$lang["gauss_cannon"] = 'Ca&ntilde;on Gauss';
$lang["ion_cannon"] = 'Ca&ntilde;on i&oacute;nico';
$lang["plasma_turret"] = 'Ca&ntilde;on de plasma';
$lang["small_shield_dome"] = 'C&uacute;pula peque&ntilde;o de protecci&oacute;n';
$lang["large_shield_dome"] = 'C&uacute;pula grande de protecci&oacute;n';

$lang["military_tech"] = 'Tec. militar';
$lang["defense_tech"] = 'Tec. de defensa';
$lang["hull_tech"] = 'Tec. de blindaje';

$lang["combustion_drive_tech"] = 'Motor de combusti&oacute;n';
$lang["impulse_drive_tech"] = 'Motor de impulso';
$lang["hyperspace_drive_tech"] = 'Propulsor hiperespacial';


$lang['theme'] = 'Tema';
$lang['report_suggestions'] = 'Envia tus sugerencias y comentarios a <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Universos de: ';
$lang['description_domain_module'] = 'Listado de universos de Ogame: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, herramientas para Ogame, tiempos de vuelo.';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, estad&iacute;sticas de %server% en %domain%';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Visión de la galaxia [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, vision de galaxia de %server% en %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Buscar Jugadores, Planetas y Alianzas (%domain%)';
$lang['description_server_search'] = 'Ogame, busqueda de datos en %server% - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Ranking (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Rankings de %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. Datos de la alianza %s% en %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, informaci&oacute;n sobre los jugadores y posici&oacute;n de la alianza %s% en %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. Datos del jugador %s% en %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, informaci&oacute;n sobre planetas y posici&oacute;n del jugador %s% en %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Comparar jugadores o alianzas. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Comparaci&oacute;n de Jugadores y alianzas en %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Gráficos estad&iacute;sticos, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, informaci&oacute;n estad&iacute;stica de %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Jugadores top %n%';
$lang['description_top_n_players'] = 'Ogniter. Listado de jugadores en el top %n%';

$lang['title_top_n_alliances'] = 'Ogniter. Alianzas top %n%';
$lang['description_top_n_alliances'] = 'Ogniter. Listado de alianzas en el top %n%';

$lang['pls_donate'] = 'Ogniter es un portal web gratuito, cuyos costos de alojamiento mensual son mayores al de un servidor web promedio.<br />
            Abro esta secci&oacute;n para los usuarios interesados en colaborar con el mantenimiento y desarrollo de mejoras en ogniter.org. Incluye tu nombre y correo!';

$lang['share'] = 'Compartir';
$lang['planets'] = 'Planetas';
$lang['moons'] = 'Lunas';

$lang['discussions'] = 'Temas de discusi&oacute;n';

$lang['find_free_slots'] = 'Localizar posiciones libres';
$lang['occupied_planets'] = 'Planetas ocupados';
$lang['find_planets'] = 'Localizar planetas';
$lang['caption'] = 'Leyenda';
$lang['free_slots_range'] = 'De %d a %d planetas ocupados';
$lang['n_occupied_planets'] = '%d planetas ocupados';

$lang['new'] = 'Nuevo';
$lang['alliance_planets'] = 'Planetas de la alianza';
$lang['members_statistics'] = 'Estad&iacute;sticas de los integrantes';

$lang['description_server_track_alliance'] = 'Busqueda de los planetas de la alianza %s%, en %server% - %domain%';
$lang['description_server_track_slots'] = 'Busqueda de posiciones libres en %server% - %domain%';
$lang['not_found_try_again'] = 'No encontrado. Por favor int&eacute;ntalo de nuevo ma&ntilde;ana.';

$lang['universe_limits'] = 'Límites del universo';
$lang['category'] = 'Categoría';
$lang['thread'] = 'Tema';

$lang['galaxy_tools'] = 'Herramientas de galaxia';
$lang['planet_search_by_status'] = 'Buscar planetas seg&uacute;n el estado del jugador';
$lang['player_status'] = "Estado del jugador";
$lang['by_player_status'] = "Buscar por estado del jugador";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Búsquedas';

$lang['find_bandits_emperors'] = 'Buscar Bandidos o Emperadores';
$lang['grand_emperor'] = 'Gran Emperador';
$lang['emperor'] = 'Emperador';
$lang['star_lord'] = 'Se&ntilde;or de las Estrellas';
$lang['bandit'] = 'Bandido';
$lang['bandit_lord'] = 'Se&ntilde;or Bandido';
$lang['bandit_king'] = 'Rey Bandido';

$lang['level'] = 'Nivel';
$lang['total_energy'] = 'Energía total';
$lang['max_temp'] = 'Max. temperatura';
$lang['energy'] = 'Energía';
$lang['results'] = 'Resultados';
$lang['resources_needed'] = 'Recursos necesarios';
$lang['calc_debris'] = 'Calcular recursos resultantes';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Cristal';
$lang['deuterium'] = 'Deuterio';

$lang['commander'] = 'Comandante';
$lang['engineer'] = 'Ingeniero';
$lang['fleet_admiral'] = 'Almirante';
$lang['geologist'] = 'Geólogo';
$lang['technocrat'] = 'Tecnócrata';

$lang['pls_enable_javascript'] = 'Debes tener javascript habilitado para poder ver algunas partes del sitio web';

$lang['top_flop'] = 'Top Subidas y Bajadas - %server% (%domain%)';
$lang['top_flop_description'] = 'Estad&aacute;sticas, mayores incrementos y ca&iacute;das en %server% (%domain%)';

$lang['tf_top_n_players'] = 'Top %n% subidas de jugadores';
$lang['tf_top_n_alliances'] = 'Top %n% subidas de alianzas';
$lang['tf_flop_n_players'] = 'Top %n% ca&iacute;das de jugadores';
$lang['tf_flop_n_alliances'] = 'Top %n% ca&iacute;das de alianzas';
$lang['top'] = 'Subidas';
$lang['flop'] = 'Caidas';

$lang['by_day'] = 'Por d&iacute;a';
$lang['faq_support'] = 'Gu&iacute;as y Soporte';
$lang['community_tools'] = 'Sitios sobre Ogame';

<?php

//Web site title
$lang['og_site_title'] = "Ogniter - Ogame Veritabanı";
//unused
$lang['og_logo_text'] = "ODB";

//"Domain", as a group of ogame related universes (ogame.fr, ogame.it, etc...)
$lang['og_domain'] = "Ülke";
$lang['og_domains'] = "Ülkeler";

//You may translate this as "Server", or "Universe"
$lang['og_server'] = "Evren";
$lang['og_servers'] = "Evrenler";

$lang['og_online_users'] = "Çevrimiçi kullanıcılar";

//Default page description
$lang['og_description'] = 'MMORPG Ogame için ücretsiz çevrimiçi veritabanı.';

$lang['og_pick_a_domain'] = "Bir Etki Alani seçin";
$lang['og_choose_a_server'] = "Bir evren seçin";
$lang['og_latest_servers'] = "En son evrenler";
$lang['screenshots'] = 'Ekran';

$lang['og_home_title'] = 'Ogniter, <small>Ogame Veritabanı</small>';
$lang['og_home_description'] = 'Tarayıcı oyunu Ogame Galaxy, sıralaması, oyuncu ve ittifak bilgileri.';
$lang['og_home_description_2'] = 'Bir galaxyrool için umurumda değil eğer, en iyi seçimdir';
$lang['og_home_how_to_start'] = 'Nasıl Başlamak için: Bir Etki Alani seçin, ve sonra listede favori evrenin seçin';
$lang['og_home_comeback'] = 'Yeni bir şey her zaman vardır. Konaklamanın keyfini çıkarabilir ve daha sonra geri gelmek.';

$lang['og_home_domain_list'] = 'Etki Alani listesi';
//Available universes
$lang['og_number_of_servers'] = '# Mevcut evrenler';

$lang['og_news'] = 'Haber';

$lang['og_terms_of_use'] = 'Kullanım Şartları';
$lang['og_privacy_policy'] = 'Gizlilik Politikası';
$lang['og_copyright'] = 'Telif hakkı';
$lang['og_developed_by'] = 'Ogniter, developed by';
$lang['og_game_created_by'] = 'Ogame is a multiplayer online game, created by Gameforge';

$lang['og_home'] = 'Ana sayfa';

$lang['top_n_players'] = 'En iyi %n% oyuncu';
$lang['top_n_alliances'] = 'En iyi %n% ittifaklar';

$lang['ogame_top_n_players'] = 'Ogame iyi %n% oyuncu';
$lang['ogame_top_n_alliances'] = 'Ogame iyi %n% ittifaklar';

//Position of the player or alliance in their universe
$lang['uni_position'] = 'Evren sıralaması';

//list of universes
$lang['og_server_list'] = 'Evren listesi';
$lang['og_server_list_from'] = '%s% yılında Evren listesi';
$lang['og_acs'] = 'ISS';
$lang['og_speed'] = 'hız';
$lang['og_def_to_debris'] = 'Enkaz Savunma';
$lang['og_num_players'] = 'Oyuncu numara';
$lang['og_num_alliances'] = 'ittifakların numara';
$lang['og_max_score'] = 'Yüsek Skor';
$lang['og_enabled'] = 'Etkin';
$lang['og_disabled'] = 'Etkin değil';

$lang['og_server_info'] = 'Evren bilgiler';
$lang['og_search'] = 'Ara';
$lang['og_galaxy'] = 'Galaksi';
$lang['og_ranking'] = 'Sıralaması';

//Unused
$lang['og_player_statuses'] = 'Oyuncular veritabanı';

//galaxy
$lang['og_galaxy_view'] = "Galaksinin görüntüle";
$lang['og_weekly_increment'] = 'Haftalık <br />artış';
$lang['og_monthly_increment'] = 'Aylık <br />artış';
$lang['og_universe'] = "Evren";
$lang['last_update'] = 'Son güncelleme';
$lang['next_update'] = 'Sonraki güncelleme (yaklaşık)';
$lang['og_system'] = 'Sistemi';
$lang['og_location'] = 'Konum';
$lang['og_planet'] = 'Gezegen';
$lang['og_moon'] = 'Ay';
$lang['og_player_status'] = 'Oyuncu (durum)';
$lang['og_position'] = 'Pozisyon';
$lang['og_alliance'] = 'Ittifak';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = '%t% önce';
//Events in the future (aproximation)
$lang['in_n_time'] = '%t% içinde';

//TO DO:
$lang['minute'] = 'Minute';
$lang['minutes'] = 'Minutes';
$lang['hour'] = 'Hour';
$lang['hours'] = 'Hours';
$lang['day'] = 'Day';
$lang['days'] = 'Days';
$lang['week'] = 'Week';
$lang['weeks'] = 'Weeks';
$lang['month'] = 'Month';
$lang['months'] = 'Months';
$lang['year'] = 'Year';
$lang['years'] = 'Years';

//server detail
$lang['og_name'] = 'Adı';
$lang['og_language'] = 'Dil';
$lang['og_timezone'] = 'Zaman dilimi';
$lang['og_ogame_version'] = 'Ogame sürüm';
$lang['og_galaxies'] = 'galaksiler';
$lang['og_systems'] = 'sistemler';
$lang['og_rapid_fire'] = 'yaylım ateşi';
$lang['og_debris_factor'] = 'haraba oranı';
$lang['og_repair_factor'] = 'tamir şansı';
$lang['og_newbie_protection_limit'] = 'Acemi koruması';
$lang['og_newbie_protection_high'] = 'Filo koruması';
$lang['og_other_info'] = 'Diğer bilgiler';
$lang['og_points_limit'] = '%d puan ile sınırlı';

//alliances

$lang['og_homepage'] = 'Anasayfa';
$lang['og_updated_on'] = 'Güncelleme..';
$lang['og_score'] = 'Skor';
$lang['statistics'] = 'Istatistik';
$lang['view'] = 'Göster';
$lang['og_members'] = 'Üyeler';
$lang['og_alliance_registration'] = 'Kayıt';
$lang['og_open'] = 'açık';
$lang['og_closed'] = 'kapandı';

//Types of ranking information
$lang['og_total'] = 'Toplam';
$lang['og_economy'] = 'Ekonomi';
$lang['og_research'] = 'Araştırma';
$lang['og_mil_points'] = 'Askeri';
$lang['og_lost_mil_points'] = 'A. kaybetti';
$lang['og_built_mil_points'] = 'inşa A.';
$lang['og_destroyed_mil_points'] = 'A. yok';
$lang['og_honor'] = 'Onur';


//player
$lang['og_player'] = 'Oyuncu';
$lang['og_num_ships'] = 'Gemi sayısı';
//Player's planets Ogniter is aware of
$lang['og_known_planets'] = 'Bilinen gezegenler';
$lang['og_type'] = 'Tip';
$lang['og_size'] = 'Boyut';

//ranking
$lang['og_results_by_alliance'] = 'Ittifak sonuçları';
$lang['og_results_by_player'] = 'Oyuncunun sonuçlar';
$lang['og_alliance_tag'] = 'Etiket';
$lang['og_average_score_per_player'] = 'Ortalama puan';

//search
$lang['og_search_by'] = 'Tarafından ara ...';
$lang['og_alliance_tag_long'] = 'Ittifak etiketi';
//Search text
$lang['og_text'] = 'Tekst';
//Submit a form
$lang['og_send'] = 'Göndermek';
$lang['og_please_do_a_db_search'] = 'Bir arama girin';
$lang['og_search_results_by'] = 'Için arama sonuçları ...';

//user status
$lang['og_inactive'] = 'Atıl';
$lang['og_inactive_30'] = 'Atıl (30 days)';
$lang['og_v_mode'] = 'Tatil modu';
$lang['og_suspended'] = 'Asma';
$lang['og_outlaw'] = 'Haydut';

//statistics
$lang['by_week'] = 'Haftasında';
$lang['by_month'] = 'Aya göre';
$lang['by_year'] = 'Yılına';
$lang['all'] = 'Tüm';

//Compare statistics
$lang['og_comparison'] = 'Karşılaştır';
$lang['compare_statistics'] = 'Oyuncuların ve ittifakların evrim karşılaştır';
$lang['search_players'] = 'Arama oyuncular';
$lang['search_alliances'] = 'Arama ittifaklar';

//Polls
$lang['vote'] = 'Oylamak';
$lang['view_results'] = 'Sonuçları görüntüle';
$lang['poll_results'] = 'Anket sonuçları';
$lang['poll_vote_ok'] = 'Oyunuz başarıyla döküm olmuştur';
$lang['poll_vote_error'] = 'Oyunuzu işlerken bir sorun oluştu';

//Colonize
$lang['og_colonize'] = 'Sömürge kurmak';

//Flight times
$lang['time_calc'] = 'Uçuş süresi hesaplayıcı';
$lang['og_flight_times'] = 'Uçuş süresi';
$lang['start'] = 'Başlangıç';
$lang['destination'] = 'Hedef';

$lang['og_updating'] = 'Güncelleniyor ...';
$lang['og_wait_a_few_seconds'] = 'Birkaç saniye bekleyin ve <a href="%s%">sonra tekrar</a> deneyin';

$lang['flight_time_calculator'] = 'Uçuş süresi hesaplaması';
$lang['required_field'] = 'Bu değer gereklidir';
$lang['integer_n_digits'] = 'Sadece tamsayı değerleri, %s% kadar basamak';
$lang['integer_only'] = 'Sadece tamsayı değerleri';
$lang['motors'] = 'Motorlar';
$lang['start_time'] = 'Başlangıç ​​saati';
$lang['fleet_ships'] = 'Gemiler';
$lang['uni_speed'] = 'Hız evren';
$lang['fleet_speed'] = 'Hız zanaat';
$lang['calc_times'] = 'Toplam süre hesaplayın';

$lang['result_time'] = 'Toplam uçuş süresi';
$lang['begin_hour'] = 'Başlangıç ​​saati';
$lang['arriving_time'] = 'Varış saati';
$lang['end_hour'] = 'Bitiş tarihi';

$lang['invalidDateTime'] = 'Geçerli bir tarih girmelisiniz';
$lang['mustAddAShip'] = 'En azından bir gemi girmelisiniz';
$lang['tools'] = 'Araçlar';

//Resources & Technologies
//Ogame API has already a translation for these. For example:
//http://uni101.ogame.fr/api/techNames.xml
$lang["small_cargo"] = 'Küçük Nakliye Gemisi';
$lang["large_cargo"] = 'Büyük Nakliye Gemisi';
$lang["light_fighter"] = 'Hafif Avcı';
$lang["heavy_fighter"] = 'Ağır Avcı';
$lang["cruiser"] = 'Kruvazör';
$lang["battle_ship"] = 'Komuta Gemisi';
$lang["colony_ship"] = 'Koloni Gemisi';
$lang["recycler"] = 'Geri Dönüsümcü';
$lang["esp_probe"] = 'Casus Sondasi';
$lang["bomber_ship"] = 'Bombardiman Gemisi';
$lang["solar_sat"] = 'Solar Uydu';
$lang["destroyer"] = 'Muhrip';
$lang["death_star"] = 'Ölüm Yildizi';
$lang["battle_cruiser"] = 'Firkateyn';
$lang["rocket_launcher"] = 'Roketatar';
$lang["light_laser"] = 'Hafif Lazer Topu';
$lang["heavy_laser"] = 'Agir Lazer Topu';
$lang["gauss_cannon"] = 'Gaus Topu';
$lang["ion_cannon"] = 'Iyon Topu';
$lang["plasma_turret"] = 'Plazma Aticilar';
$lang["small_shield_dome"] = 'Küçük Kalkan Kubbesi';
$lang["large_shield_dome"] = 'Büyük Kalkan Kubbesi';

$lang["military_tech"] = 'Silah Teknigi';
$lang["defense_tech"] = 'Koruyucu Kalkan Teknigi';
$lang["hull_tech"] = 'Uzay Gemilerinin Zirhlandirilmasi';

$lang["combustion_drive_tech"] = 'Yanmali Motor Takimi';
$lang["impulse_drive_tech"] = 'Impuls( Içtepi ) Motortakimi';
$lang["hyperspace_drive_tech"] = 'Hiperuzay Iticisi';
//

$lang['theme'] = 'Sunuş';
$lang['report_suggestions'] = 'Send your suggestions and comments to <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Dan Evrenler: ';
$lang['description_domain_module'] = 'Ogame evrenler listesi: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Ogame araçları. Ogame uçuş süresi hesaplayıcı';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, evren %server% - %domain% (statistics)';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Galaksinin görüntüle [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, evren %server% (galaksinin görüntüle) - %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Arama Oyuncular, Gezegenler ve Birlikler (%domain%)';
$lang['description_server_search'] = 'Ogame, Evren %server% (arama bilgi sayfası) - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Sıralaması (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Dan Sıralaması puanları %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. %s% gerçekler ittifak, %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Sıralama bilgi ve %s% oyuncu listesi, %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. %s% oyuncu verileri, %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, Bilgi ve gezegenin %s% Sıralama listesi, %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Oyuncular ya Birlikler karşılaştırın. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Oyuncular & Birlikler Karşılaştırılması, %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Istatiksel grafikler, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, %server% İstatistik bilgi - %domain%';

$lang['title_top_n_players'] = 'Ogniter. En iyi %n% oyuncu';
$lang['description_top_n_players'] = 'Ogniter. En iyi %n% oyuncu (Sıralaması)';

$lang['title_top_n_alliances'] = 'Ogniter. En iyi %n% ittifaklar';
$lang['description_top_n_alliances'] = 'Ogniter. En iyi %n% ittifaklar (Sıralaması)';

$lang['pls_donate'] = 'Ogniter is a free website, with monthly payment costs above the average web-hosting package.<br />
            We are accepting now donations of users who wish to cooperate with the maintenance, and the development of new features on ogniter.org.<br /> Don\'t forget to include your name and email!';

$lang['share'] = 'Hisse';
$lang['planets'] = 'Gezegenler';
$lang['moons'] = '# Ay';

$lang['discussions'] = 'Forumlar';

$lang['find_free_slots'] = 'Ücretsiz pozisyonları bul';
$lang['occupied_planets'] = 'İşgal gezegenler';
$lang['find_planets'] = 'Gezegenler bul';
$lang['caption'] = 'Tanım';
$lang['free_slots_range'] = '%d ila %d işgal gezegenler Gönderen';
$lang['n_occupied_planets'] = '%d işgal gezegenler';

$lang['new'] = 'Yeni';
$lang['alliance_planets'] = 'İttifak\'ın gezegenler';
$lang['members_statistics'] = 'Üye istatistikleri';

$lang['description_server_track_alliance'] = 'Ittifak %s% gezegenler arama, %server% - %domain%';
$lang['description_server_track_slots'] = '%server% ücretsiz pozisyonlar için ara - %domain%';
$lang['not_found_try_again'] = 'Bulunamadı. Lütfen yarın tekrar deneyin.';

$lang['universe_limits'] = 'Evrenin Sınırları';
$lang['category'] = 'Kategori';
$lang['thread'] = 'Konu';

$lang['galaxy_tools'] = 'Galaxy araçları';
$lang['planet_search_by_status'] = 'Oyuncunun durumuna göre gezegenleri bulmak';
$lang['player_status'] = "Oyuncunun durumu";
$lang['by_player_status'] = "Oyuncunun durumuna göre bul";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Aramalar';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

<?php

$lang['og_site_title'] = "Ogniter - Ogame baza danych on-line";
$lang['og_logo_text'] = "ODB";
$lang['og_domain'] = "Domena";
$lang['og_domains'] = "Domeny";
$lang['og_server'] = "Wszechświat";
$lang['og_servers'] = "Wszechświaty";
$lang['og_online_users'] = "Użytkownicy online";

$lang['og_description'] = 'Darmowe bazy danych dla multiplayer gry Ogame. Narzędzia dla Ogame';

$lang['og_pick_a_domain'] = "Wybierz domenę";
$lang['og_choose_a_server'] = "Wybierz wszechświat";
$lang['og_latest_servers'] = "Najnowsze wszechświaty";
$lang['screenshots'] = 'Zrzuty ekranu';

$lang['og_home_title'] = 'Ogniter, <small>Ogame baza danych</small>';
$lang['og_home_description'] = 'Galaxy, rankingi, zawodnicy i informacje o sojuszu, z przeglądarką gry Ogame.';
$lang['og_home_description_2'] = 'Najlepszym wyborem, jeśli nie masz Galaxytool niedostępna.';
$lang['og_home_how_to_start'] = 'Jak zacząć: <strong>wybierz</strong> domenę, a następnie wybierz swój ulubiony wszechświat w liście';
$lang['og_home_comeback'] = 'Zawsze jest coś nowego. Miłego pobytu i wrócić później.';

$lang['og_home_domain_list'] = 'Lista domen';
//Available universes
$lang['og_number_of_servers'] = '# dostępne wszechświaty';

$lang['og_news'] = 'Aktualności';

$lang['og_terms_of_use'] = 'Warunki korzystania z serwisu';
$lang['og_privacy_policy'] = 'Polityka prywatności';
$lang['og_copyright'] = 'Prawo autorskie';
$lang['og_developed_by'] = 'Ogniter, opracowany przez ';
$lang['og_game_created_by'] = 'Ogame to gra online multiplayer, stworzony przez Gameforge';

$lang['og_home'] = 'Home site';

$lang['top_n_players'] = 'Najlepszych %n% graczy';
$lang['top_n_alliances'] = 'Najlepszych %n% sojuszy';

$lang['ogame_top_n_players'] = 'Top %n% graczy na Ogame';
$lang['ogame_top_n_alliances'] = 'Top %n% sojuszy na Ogame';
$lang['uni_position'] = 'Wszechświat pozycji';

//server list
$lang['og_server_list'] = 'Lista wszechświat';
$lang['og_server_list_from'] = 'Lista wszechświat od %s% domen';
$lang['og_acs'] = 'AKS';
$lang['og_speed'] = 'Prędkość';
$lang['og_def_to_debris'] = 'Obrona do pola szczątków';
$lang['og_num_players'] = '# graczy';
$lang['og_num_alliances'] = '# sojuszy';
$lang['og_max_score'] = 'Maksymalna ocena';
$lang['og_enabled'] = 'Włączony';
$lang['og_disabled'] = 'Poza';

//servers module
$lang['og_server_info'] = 'Dane wszechświat';
$lang['og_search'] = 'Szukaj';
$lang['og_player_statuses'] = 'Państwo z graczy';
$lang['og_galaxy'] = 'Galaktyka';
$lang['og_ranking'] = 'Zaszeregowanie';

//galaxy
$lang['og_galaxy_view'] = "Wizja galaktyki";
$lang['og_weekly_increment'] = 'Przyrost<br />tygodniowy';
$lang['og_monthly_increment'] = 'miesięczny<br />wzrost';
$lang['og_universe'] = "Wszechświat";
$lang['last_update'] = 'Ostatnia aktualizacja';
$lang['next_update'] = 'Następna aktualizacja (około.)';
$lang['og_system'] = 'Układ';
$lang['og_location'] = 'Lokalizacja';
$lang['og_planet'] = 'Planeta';
$lang['og_moon'] = 'Księżyc';
$lang['og_player_status'] = 'Gracz (status)';
$lang['og_position'] = 'Pozycja';
$lang['og_alliance'] = 'Sojuszy';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = '%t% temu';
//Events in the future (aproximation)
$lang['in_n_time'] = 'w %t%';

$lang['second'] = 'sekunda';
$lang['seconds'] = 'sekundy';
$lang['minute'] = 'minuta';
$lang['minutes'] = 'minuty';
$lang['hour'] = 'godzina';
$lang['hours'] = 'godzin';
$lang['day'] = 'dzień';
$lang['days'] = 'dni';
$lang['week'] = 'tydzień';
$lang['weeks'] = 'tygodnie';
$lang['month'] = 'miesiąc';
$lang['months'] = 'miesiące';
$lang['year'] = 'rok';
$lang['years'] = 'lata';

//server detail
$lang['og_name'] = 'Nazwa';
$lang['og_language'] = 'Język';
$lang['og_timezone'] = 'Strefa czasu';
$lang['og_ogame_version'] = 'Wersja Ogame';
$lang['og_galaxies'] = 'Galaktyki';
$lang['og_systems'] = 'Układy';
$lang['og_rapid_fire'] = 'Szybkie działa';
$lang['og_debris_factor'] = 'Współczynnik gruzu';
$lang['og_repair_factor'] = 'Obrona Naprawa';
$lang['og_newbie_protection_limit'] = 'Ochrona nowy';
$lang['og_newbie_protection_high'] = 'Ochrona statków';
$lang['og_other_info'] = 'Inne informacje';
$lang['og_points_limit'] = 'Do %d punktów';

//alliance
$lang['og_homepage'] = 'Strona główna';
$lang['og_updated_on'] = 'Zaktualizowane';
$lang['og_score'] = 'Punkty';
$lang['statistics'] = 'Statystyka';
$lang['view'] = 'Zobaczyć';

$lang['og_total'] = 'łączny';
$lang['og_economy'] = 'Gospodarka';
$lang['og_research'] = 'Badania';
$lang['og_mil_points'] = 'Wojskowe punkty';
$lang['og_lost_mil_points'] = 'W. P: porażka';
$lang['og_built_mil_points'] = 'P. Ml. wbudowany';
$lang['og_destroyed_mil_points'] = 'W. P.: zniszczone';
$lang['og_honor'] = 'Cześć';
$lang['og_members'] = 'Członków';
$lang['og_alliance_registration'] = 'Rekrutacja';
$lang['og_open'] = 'otwartej rekrutacji';
$lang['og_closed'] = 'zamknięta rekrutacja';

//player
$lang['og_player'] = 'Gracz';
$lang['og_num_ships'] = '# Statki';
$lang['og_known_planets'] = 'Znane planety';
$lang['og_type'] = 'Typ';
$lang['og_size'] = 'Wielkości';

//ranking
$lang['og_results_by_alliance'] = 'Wyniki według sojuszu';
$lang['og_results_by_player'] = 'Wyniki według gracza';
$lang['og_alliance_tag'] = 'Etykietka';
$lang['og_average_score_per_player'] = 'średnia na gracza';

//search

$lang['og_search_by'] = 'Szukaj według';
$lang['og_alliance_tag_long'] = 'Sojusz etykietka';
$lang['og_text'] = 'Tekst';
$lang['og_send'] = 'Wyszukaj';
$lang['og_please_do_a_db_search'] = 'Wpisz wyszukiwanie';
$lang['og_search_results_by'] = 'Wyniki wyszukiwania dla ...';

//status
$lang['og_inactive'] = 'Nieaktywne';
$lang['og_inactive_30'] = 'Nieaktywne (30 dias)';
$lang['og_v_mode'] = 'W wolnych';
$lang['og_suspended'] = 'Zawieszony';
$lang['og_outlaw'] = 'Banita';

//statistics
$lang['by_week'] = 'Tygodniowy';
$lang['by_month'] = 'Na miesiąc';
$lang['by_year'] = 'Na rok';
$lang['all'] = 'Wszystko';

//Compare statistics
$lang['og_comparison'] = 'Porównanie';
$lang['compare_statistics'] = 'Porównaj ewolucji graczy i sojuszy';
$lang['search_players'] = 'Szukaj gracze';
$lang['search_alliances'] = 'Szukaj sojuszu';

//Polls
$lang['vote'] = 'Głosować';
$lang['view_results'] = 'Zobacz wyniki';
$lang['poll_results'] = 'Wyniki ankiety';
$lang['poll_vote_ok'] = 'Twój głos został pomyślnie zarejestrowany.';
$lang['poll_vote_error'] = 'Nie było problemu, podczas przetwarzania głos';

//Colonize
$lang['og_colonize'] = 'Kolonizować';

//Flight times
$lang['time_calc'] = 'Oblicz czas lotu';
$lang['og_flight_times'] = 'Czas lotu';
$lang['start'] = 'Punkt wyjścia';
$lang['destination'] = 'Cel';

$lang['og_updating'] = 'Aktualizowanie ...';
$lang['og_wait_a_few_seconds'] = 'Poczekaj kilka sekund ... a następnie <a href="%s%">spróbuj ponownie</a>';

$lang['flight_time_calculator'] = 'Oblicz czas lotu';
$lang['required_field'] = 'Musisz wypełnić wartośćr';
$lang['integer_n_digits'] = 'Wartość musi być liczbą całkowitą do %s% cyfr';
$lang['integer_only'] = 'Tylko wartości całkowite';
$lang['motors'] = 'Silniki';
$lang['start_time'] = 'Czas rozpoczęcia';
$lang['fleet_ships'] = 'Statki należące do floty';
$lang['uni_speed'] = 'Prędkość wszechświat';
$lang['fleet_speed'] = 'Statek pasażerski';
$lang['calc_times'] = 'Oblicz razy';

$lang['result_time'] = 'Czas lotu wynikająca';
$lang['begin_hour'] = 'Czas rozpoczęcia';
$lang['arriving_time'] = 'Sprawdzać';
$lang['end_hour'] = 'Czas powrotu';

$lang['invalidDateTime'] = 'Musisz wprowadzić prawidłową datę';
$lang['mustAddAShip'] = 'Musisz podać co najmniej jeden statek';
$lang['tools'] = 'Narzędzia';

//Resources & Technologies
$lang["small_cargo"] = 'Mały transporter';
$lang["large_cargo"] = 'Duży transporter';
$lang["light_fighter"] = 'Lekki myśliwiec';
$lang["heavy_fighter"] = 'Ciężki myśliwiec';
$lang["cruiser"] = 'Krążownik';
$lang["battle_ship"] = 'Okręt wojenny';
$lang["colony_ship"] = 'Statek kolonizacyjny';
$lang["recycler"] = 'Recykler';
$lang["esp_probe"] = 'Sonda szpiegowska';
$lang["bomber_ship"] = 'Bombowiec';
$lang["solar_sat"] = 'Satelita słoneczny';
$lang["destroyer"] = 'Niszczyciel';
$lang["death_star"] = 'Gwiazda Śmierci';
$lang["battle_cruiser"] = 'Pancernik';
$lang["rocket_launcher"] = 'Wyrzutnia rakiet';
$lang["light_laser"] = 'Lekkie działo laserowe';
$lang["heavy_laser"] = 'Ciężkie działo laserowe';
$lang["gauss_cannon"] = 'Działo Gaussa';
$lang["ion_cannon"] = 'Działo jonowe';
$lang["plasma_turret"] = 'Wyrzutnia plazmy';
$lang["small_shield_dome"] = 'Mała powłoka ochronna';
$lang["large_shield_dome"] = 'Duża powłoka ochronna';

$lang["military_tech"] = 'Technologia bojowa';
$lang["defense_tech"] = 'Technologia ochronna';
$lang["hull_tech"] = 'Opancerzenie';

$lang["combustion_drive_tech"] = 'Napęd spalinowy';
$lang["impulse_drive_tech"] = 'Napęd impulsowy';
$lang["hyperspace_drive_tech"] = 'Napęd nadprzestrzenny';


$lang['theme'] = 'Prezentacja';
$lang['report_suggestions'] = 'Send your suggestions and comments to <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Wszechświaty w:';
$lang['description_domain_module'] = 'Ogame, wszechświaty listę:';
$lang['description_fleet_simulator_module'] = 'Ogniter, narzędzia Ogame, godziny lotu.';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, Statystyki %server% w %domain%';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Wizja galaktyki [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, %server% galaxy wizja w %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Szukaj Player, Planety i Sojusze (%domain%)';
$lang['description_server_search'] = 'Ogame, Wyszukiwanie danych w %server% - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Zaszeregowanie (%domain%)';
$lang['description_server_ranking'] = 'Ogame, %server% rankingi - %domain%';

$lang['title_server_alliance'] = 'Ogniter. Informacje %s% sojusz w %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, informacje o graczach i stanowisko sojuszu %s% w %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. Informacje %s% gracz w %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, informacje na temat planet i pozycji gracza %s% w %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Porównaj tych graczy lub sojuszy. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Gracze porównywarka i sojusze w uniwersum %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Grafika statystyczne, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, informacje statystyczne dotyczące %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Najlepszych %n% graczy';
$lang['description_top_n_players'] = 'Ogniter. Lista Graczy w górę %n%';

$lang['title_top_n_alliances'] = 'Ogniter. Najlepszych %n% sojuszy';
$lang['description_top_n_alliances'] = 'Ogniter. Lista sojuszy w górę %n%';

$lang['pls_donate'] = 'Ogniter is a free access website, with monthly payment costs above the average web-hosting package.<br />
            We are accepting now donations of users who wish to cooperate with the maintenance, and the development of new features on ogniter.org.<br /> Don\'t forget to include your name and email!';

$lang['share'] = 'Udział';
$lang['planets'] = 'Planety';
$lang['moons'] = 'Księżyc';

$lang['discussions'] = 'Tematy dyskusji';

$lang['find_free_slots'] = 'Znaleźć wolne stanowiska';
$lang['occupied_planets'] = 'Okupowane planety';
$lang['find_planets'] = 'Znajdź planety';
$lang['caption'] = 'Napis';
$lang['free_slots_range'] = 'Od %d do %d okupowanych planet';
$lang['n_occupied_planets'] = '%d planety zajęte';

$lang['new'] = 'Nowy';
$lang['alliance_planets'] = 'Sojuszu planety';
$lang['members_statistics'] = 'Statystyki posłów';

$lang['description_server_track_alliance'] = 'Planety search z sojuszu %s%, %server% - %domain%';
$lang['description_server_track_slots'] = 'Szukaj wolnych stanowisk w %server% - %domain%';
$lang['not_found_try_again'] = 'Nie znaleziono. Prosimy spróbować ponownie jutro.';

$lang['universe_limits'] = 'Granice wszechświata';
$lang['category'] = 'Kategoria';
$lang['thread'] = 'Temat';

$lang['galaxy_tools'] = 'Narzędzia Galaxy';
$lang['planet_search_by_status'] = 'Planety wyszukiwania według statusu gracza';
$lang['player_status'] = "Status gracza";
$lang['by_player_status'] = "Znajdź według statusu gracza";
$lang['normal'] = 'Normalny';
$lang['popular_searches'] = 'Wyszukiwania';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

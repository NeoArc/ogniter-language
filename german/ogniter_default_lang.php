<?php

//By Thath0r

$lang['og_site_title'] = "Ogniter - Ogame-Datenbank";
$lang['og_logo_text'] = "ODB";
$lang['og_domain'] = "Land";
$lang['og_domains'] = "Länderliste";
$lang['og_server'] = "Universum";
$lang['og_servers'] = "Universen";
$lang['og_online_users'] = "online-Benutzer";

$lang['og_description'] = 'Datenbank der Ogame-Universen. Galaxieansicht, Highscore. Werkzeuge für Ogame';

$lang['og_pick_a_domain'] = "Wähle ein Land";
$lang['og_choose_a_server'] = "Wähle ein Universum";
$lang['og_latest_servers'] = "Neueste Universen";
$lang['screenshots'] = 'Screenshots';

$lang['og_home_title'] = 'Ogniter, <small>Ogame-Datenbank</small>';
$lang['og_home_description'] = 'Informationen über Spieler, Allianzen und Planeten im Browsergame Ogame.';
$lang['og_home_description_2'] = 'Die beste Option wenn du auf das Galaxytool verzichten willst.';
$lang['og_home_how_to_start'] = 'Um anzufangen, wähle ein Land und dein gewünschtes Universum aus der Liste.';
$lang['og_home_comeback'] = 'Ogniter wird regelmässig aktualisiert - komm bald wieder!';

$lang['og_home_domain_list'] = 'Länderliste';
$lang['og_number_of_servers'] = '# von registrierten Universen';
$lang['og_news'] = 'Neuigkeiten';

$lang['og_terms_of_use'] = 'Nutzungsbedingungen';
$lang['og_privacy_policy'] = 'Datenschutz';
$lang['og_copyright'] = 'Copyright';
$lang['og_developed_by'] = 'Ogniter, entwickelt und betrieben von';
$lang['og_game_created_by'] = 'Ogame ist ein Browserspiel der Gameforge AG';

$lang['og_home'] = 'Startseite';

$lang['top_n_players'] = 'Top %n% Spieler';
$lang['top_n_alliances'] = 'Top %n% Allianzen';

$lang['ogame_top_n_players'] = 'Ogame Top %n% Spieler';
$lang['ogame_top_n_alliances'] = 'Ogame Top %n% Allianzen';
$lang['uni_position'] = 'Rang im Universum';

//server list
$lang['og_server_list'] = 'Liste der Universen';
$lang['og_server_list_from'] = 'Liste der %s% -Universen';
$lang['og_acs'] = 'AKS';
$lang['og_speed'] = 'Speedfaktor';
$lang['og_def_to_debris'] = 'Def ins TF';
$lang['og_num_players'] = '# Spieler';
$lang['og_num_alliances'] = '# Allianzen';
$lang['og_max_score'] = 'Maximale Punktzahl';
$lang['og_enabled'] = 'Aktiviert';
$lang['og_disabled'] = ' Deaktiviert';

//servers module
$lang['og_server_info'] = 'Universumsdetails';
$lang['og_search'] = 'Suchen';
$lang['og_player_statuses'] = 'Spielerstatus';
$lang['og_galaxy'] = 'Galaxie';
$lang['og_ranking'] = 'Highscore';

//galaxy
$lang['og_galaxy_view'] = "Galaxieansicht";
$lang['og_weekly_increment'] = 'Wöchentliche Zunahme';
$lang['og_monthly_increment'] = 'Monatliche Zunahme';
$lang['og_universe'] = "Universum";
$lang['last_update'] = 'Zuletzt aktualisiert';
$lang['next_update'] = 'Nächstes Update (ca.)';
$lang['og_system'] = 'Sonnensystem';
$lang['og_location'] = 'Position';
$lang['og_planet'] = 'Planet';
$lang['og_moon'] = 'Mond';
$lang['og_player_status'] = 'Spieler (Ansehen)';
$lang['og_position'] = 'Rang';
$lang['og_alliance'] = 'Allianz';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = 'Vor %time%';
//Events in the future (aproximation)
$lang['in_n_time'] = 'In %time% (ca.)';

$lang['second'] = 'Sekunde';
$lang['seconds'] = 'Sekunden';
$lang['minute'] = 'Minute';
$lang['minutes'] = 'Minuten';
$lang['hour'] = 'Stunde';
$lang['hours'] = 'Stunden';
$lang['day'] = 'Tag';
$lang['days'] = 'Tage';
$lang['week'] = 'Woche';
$lang['weeks'] = 'Wochen';
$lang['month'] = 'Monat';
$lang['months'] = 'Monate';
$lang['year'] = 'Jahr';
$lang['years'] = 'Jahre';

//server detail
$lang['og_name'] = 'Name';
$lang['og_language'] = 'Sprache';
$lang['og_timezone'] = 'Zeitzone';
$lang['og_ogame_version'] = 'Ogame Version';
$lang['og_galaxies'] = 'Galaxien';
$lang['og_systems'] = 'Sonnensysteme';
$lang['og_rapid_fire'] = 'RapidFire';
$lang['og_debris_factor'] = 'Strukturpunkte ins TF';
$lang['og_repair_factor'] = 'Reparaturfaktor der Verteidigung';
$lang['og_newbie_protection_limit'] = 'Noobschutz';
$lang['og_newbie_protection_high'] = 'Flottenflucht';
$lang['og_other_info'] = 'Andere Daten';
$lang['og_points_limit'] = 'Bis %d Punkte';

//alliance
$lang['og_homepage'] = 'Startseite';
$lang['og_updated_on'] = 'Aktualisiert am';
$lang['og_score'] = 'Punkte';
$lang['statistics'] = 'Statistiken';
$lang['view'] = 'Anzeigen';

$lang['og_total'] = 'Punkte';
$lang['og_economy'] = 'Ökonomie';
$lang['og_research'] = 'Forschung';
$lang['og_mil_points'] = 'Militär';
$lang['og_lost_mil_points'] = 'Militärpunkte verloren';
$lang['og_built_mil_points'] = 'Militärpunkte gebaut';
$lang['og_destroyed_mil_points'] = 'Militärpunkte zerstört';
$lang['og_honor'] = 'Ehrenpunkte';
$lang['og_members'] = 'Mitglieder';
$lang['og_alliance_registration'] = 'Bewerbung';
$lang['og_open'] = 'möglich';
$lang['og_closed'] = 'Allianz geschlossen';

//player
$lang['og_player'] = 'Spieler';
$lang['og_num_ships'] = '# Schiffe';
$lang['og_known_planets'] = 'Bekannte Planeten';
$lang['og_type'] = 'Typ';
$lang['og_size'] = 'Größe';

//ranking
$lang['og_results_by_alliance'] = 'Allianz-Highscore';
$lang['og_results_by_player'] = 'Spieler-Highscore';
$lang['og_alliance_tag'] = 'Tag';
$lang['og_average_score_per_player'] = 'Durchschnitt pro Spieler';

//search

$lang['og_search_by'] = 'Suche nach';
$lang['og_alliance_tag_long'] = 'Allianztag';
$lang['og_text'] = 'Text';
$lang['og_send'] = 'Senden';
$lang['og_please_do_a_db_search'] = 'Bitte gib einen Suchbegriff ein';
$lang['og_search_results_by'] = 'Suchergebnis nach';

//status
$lang['og_inactive'] = 'Inaktiv';
$lang['og_inactive_30'] = 'Inaktiv (30 Tage)';
$lang['og_v_mode'] = 'Urlaubsmodus';
$lang['og_suspended'] = 'gesperrt';
$lang['og_outlaw'] = 'Vogelfreier';

//statistics
$lang['by_week'] = 'pro Woche';
$lang['by_month'] = 'pro Monat';
$lang['by_year'] = 'pro Jahr';
$lang['all'] = 'Alles';

//Compare statistics
//$lang['og_comparison'] = 'Fortschritt vergleichen';
$lang['og_comparison'] = 'Vergleichen';
$lang['compare_statistics'] = 'Vergleiche die Fortschritte der Spieler und Allianzen';
$lang['search_players'] = 'Suche Spieler';
$lang['search_alliances'] = 'Suche Allianzen';

//Polls
$lang['vote'] = 'Abstimmen';
$lang['view_results'] = 'Ergebnisse anzeigen';
$lang['poll_results'] = 'Ergebnisse der Umfrage';
$lang['poll_vote_ok'] = 'Deine Stimme wurde erfolgreich gespeichert.';
$lang['poll_vote_error'] = 'Es gab ein Problem bei der Verarbeitung deiner Stimme';

//Colonize
$lang['og_colonize'] = 'Kolonisieren';

//Flight times
$lang['time_calc'] = 'Flugzeit berechnen';
$lang['og_flight_times'] = 'Flugzeiten';
$lang['start'] = 'Startposition';
$lang['destination'] = 'Ziel';

$lang['og_updating'] = 'Aktualisierung...';
$lang['og_wait_a_few_seconds'] = 'Bitte warten einige Sekunden und versuche dann, <a href="%s%">die Seite erneut aufrufen.</a>';

$lang['flight_time_calculator'] = 'Flugzeitrechner';
$lang['required_field'] = 'Obligatorisches Feld';
$lang['integer_n_digits'] = 'Der Wert muss eine ganze Zahl mit maximal %s% Ziffern sein';
$lang['integer_only'] = 'Nur ganze Zahlen sind erlaubt';
$lang['motors'] = 'Antriebsforschungen';
$lang['start_time'] = 'Startzeit';
$lang['fleet_ships'] = 'Schiffe in der Flotte';
$lang['uni_speed'] = 'Geschwindigkeit des Universums';
$lang['fleet_speed'] = 'Geschwindigkeit der Schiffe';
$lang['calc_times'] = 'Berechnen';

$lang['result_time'] = 'Flugzeit';
$lang['begin_hour'] = 'Startzeit';
$lang['arriving_time'] = 'Ankunftszeit';
$lang['end_hour'] = 'Rückrufzeit';

$lang['invalidDateTime'] = 'Du musst ein gültiges Datum eingeben';
$lang['mustAddAShip'] = 'Du musst mindestens ein Schiff versenden!';
$lang['tools'] = 'Werkzeuge';

//Resources & Technologies
$lang["small_cargo"] = 'Kleiner Transporter';
$lang["large_cargo"] = 'Großer Transporter';
$lang["light_fighter"] = 'Leichter Jäger';
$lang["heavy_fighter"] = 'Schwerer Jäger';
$lang["cruiser"] = 'Kreuzer';
$lang["battle_ship"] = 'Schlachtschiff';
$lang["colony_ship"] = 'Kolonieschiff';
$lang["recycler"] = 'Recycler';
$lang["esp_probe"] = 'Spionagesonde';
$lang["bomber_ship"] = 'Bomber';
$lang["solar_sat"] = 'Solarsatellit';
$lang["destroyer"] = 'Zerstörer';
$lang["death_star"] = 'Todesstern';
$lang["battle_cruiser"] = 'Schlachtkreuzer';
$lang["rocket_launcher"] = 'Raketenwerfer';
$lang["light_laser"] = 'Leichtes Lasergeschütz';
$lang["heavy_laser"] = 'Schweres Lasergeschütz';
$lang["gauss_cannon"] = 'Gaußkanone';
$lang["ion_cannon"] = 'Ionengeschütz';
$lang["plasma_turret"] = 'Plasmawerfer';
$lang["small_shield_dome"] = 'Kleine Schildkuppel';
$lang["large_shield_dome"] = 'Große Schildkuppel';

$lang["military_tech"] = 'Waffentechnik';
$lang["defense_tech"] = 'Schildtechnik';
$lang["hull_tech"] = 'Raumschiffpanzerung';

$lang["combustion_drive_tech"] = 'Verbrennungstriebwerk';
$lang["impulse_drive_tech"] = 'Impulstriebwerk';
$lang["hyperspace_drive_tech"] = 'Hyperraumantrieb';


$lang['theme'] = 'Skin';
$lang['report_suggestions'] = 'Sende deine Vorschläge und Kommentare an: <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Universen: ';
$lang['description_domain_module'] = 'Liste der Ogame-Universen ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Ogame Werkzeuge, Flugzeiten.';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, %server% Statistiken %domain%';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Galaxieansicht [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Galaxieansicht %server% %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Suche Spieler, Planeten und Allianzen (%domain%)';
$lang['description_server_search'] = 'Ogame, Suche auf %server% - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Highscore (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Highscore für %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. Allianz %s% für %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Informationen über die Spieler und die Position der Allianz %s% e %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. Der Spieler %s%, %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, Informationen über Planeten und Spieler %s% Position, %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Vergleichen Spielern oder Allianzen. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Vergleich von Spieler und Allianzen, %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Statistische Grafiken, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, statistische Informationen für %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Top %n% Spieler';
$lang['description_top_n_players'] = 'Ogniter. Liste der Spieler in den Top %n%';

$lang['title_top_n_alliances'] = 'Ogniter. Top %n% Allianz';
$lang['description_top_n_alliances'] = 'Ogniter. Liste der Allianzen in den %n%';

$lang['pls_donate'] = 'Ogniter ist eine gratis Webseite mit erheblichen monatlichen Ausgaben. Wenn dir Ogniter gefällt und du Ogniter regelmässig benutzt, bitte Spende uns doch einen kleinen Beitrag.<br />
            Falls du beim Unterhalt oder der Entwicklung von neuen features mithelfen möchtest, melde dich!<br /> Vergiss nicht, deinen Namen und Emailadresse zu erwähnen!';

$lang['share'] = 'Teilen';
$lang['planets'] = 'Planeten';
$lang['moons'] = 'Monde';

$lang['discussions'] = 'Diskussionsthemen';

$lang['find_free_slots'] = 'Verfügbare Positionen';
$lang['occupied_planets'] = 'Besetzte Planeten';
$lang['find_planets'] = 'Suche Planeten';
$lang['caption'] = 'Beschriftung';
$lang['free_slots_range'] = 'Von %d bis %d besetzte Planeten';
$lang['n_occupied_planets'] = '%d besetzte Planeten';

$lang['new'] = 'Neu';
$lang['alliance_planets'] = 'Allianzplaneten';
$lang['members_statistics'] = 'Mitgliederstatistik';

$lang['description_server_track_alliance'] = 'Suche nach Planeten der Allianz %s%, %server% - %domain%';
$lang['description_server_track_slots'] = 'Suche nach freien Positionen in %server% - %domain%';
$lang['not_found_try_again'] = 'Nicht gefunden. Bitte versuche es morgen noch einmal.';

$lang['universe_limits'] = 'Grösse des Universums';
$lang['category'] = 'Kategorie';
$lang['thread'] = 'Thema';

$lang['galaxy_tools'] = 'Galaxie-Tools';
$lang['planet_search_by_status'] = 'Suche nach Planeten von Spieler \'s Status';
$lang['player_status'] = "Spieler-Status";
$lang['by_player_status'] = "von Spieler \'s Status";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Suchanfragen';

$lang['find_bandits_emperors'] = 'Finde Banditen/Imperatoren';
$lang['grand_emperor'] = 'Großimperator';
$lang['emperor'] = 'Imperator';
$lang['star_lord'] = 'Starlord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Banditenlord';
$lang['bandit_king'] = 'Banditenkönig';

$lang['level'] = 'Position';
$lang['total_energy'] = 'Gesamte Energie';
$lang['max_temp'] = 'Max. Temperatur';
$lang['energy'] = 'Energie';
$lang['results'] = 'Resultate';
$lang['resources_needed'] = 'Benötigte Ressourcen';
$lang['calc_debris'] = 'Berechne resultierendes Trümmerfeld';

$lang['metal'] = 'Metall';
$lang['crystal'] = 'Kristall';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Ingenieur';
$lang['fleet_admiral'] = 'Admiral';
$lang['geologist'] = 'Geologe';
$lang['technocrat'] = 'Technokrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

<?php

//Web site title
$lang['og_site_title'] = "Ogniter - Ogame Database";
//unused
$lang['og_logo_text'] = "ODB";

//"Domain", as a group of ogame related universes (ogame.fr, ogame.it, etc...)
$lang['og_domain'] = "Domain";
$lang['og_domains'] = "Domains";

//You may translate this as "Server", or "Universe"
$lang['og_server'] = "Universe";
$lang['og_servers'] = "Universes";

$lang['og_online_users'] = "Online users";

//Default page description
$lang['og_description'] = 'Free online database for the browser game Ogame. Cartography, statistics &amp; tools';

$lang['og_pick_a_domain'] = "Pick a domain";
$lang['og_choose_a_server'] = "Select a universe";
$lang['og_latest_servers'] = "Latest universes";
$lang['screenshots'] = 'Screenshots';

$lang['og_home_title'] = 'Ogniter, <small>online Ogame database</small>';
$lang['og_home_description'] = 'Galaxy cartography, rankings, players &amp; alliance info';
$lang['og_home_description_2'] = 'The best choice if you dont have a Galaxytool available.';
$lang['og_home_how_to_start'] = 'How to get started: <strong>select</strong> a domain, and then <strong>choose</strong> your favorite universe in the list';
$lang['og_home_comeback'] = 'There\'s always something new. Enjoy your stay and come back later.';

$lang['og_home_domain_list'] = 'Domain list';
//Available universes
$lang['og_number_of_servers'] = '# of registered servers';

$lang['og_news'] = 'What\'s new?';

$lang['og_terms_of_use'] = 'Terms of Use';
$lang['og_privacy_policy'] = 'Privacy Policy';
$lang['og_copyright'] = 'Copyright';
$lang['og_developed_by'] = 'Ogniter, developed and maintained by';
$lang['og_game_created_by'] = 'Ogame is a multiplayer online game, created by Gameforge';

$lang['og_home'] = 'Home';

$lang['top_n_players'] = 'Top %n% Players';
$lang['top_n_alliances'] = 'Top %n% Alliances';

$lang['ogame_top_n_players'] = 'World Top %n% Players';
$lang['ogame_top_n_alliances'] = 'World Top %n% Alliances';

//Position of the player or alliance in their universe
$lang['uni_position'] = 'Uni ranking';

//list of universes
$lang['og_server_list'] = 'Universe list';
$lang['og_server_list_from'] = 'Universe list in %s%';
$lang['og_acs'] = 'ACS';
$lang['og_speed'] = 'Speed';
$lang['og_def_to_debris'] = 'Defense to debris';
$lang['og_num_players'] = '# of players';
$lang['og_num_alliances'] = '# of alliances';
$lang['og_max_score'] = 'Max score';
$lang['og_enabled'] = 'Enabled';
$lang['og_disabled'] = 'Disabled';

$lang['og_server_info'] = 'Server information';
$lang['og_search'] = 'Search';
$lang['og_galaxy'] = 'Galaxy';
$lang['og_ranking'] = 'Ranking';

//galaxy
$lang['og_galaxy_view'] = "Galaxy view";
$lang['og_weekly_increment'] = 'Weekly <br />increment';
$lang['og_monthly_increment'] = 'Monthly <br />increment';
$lang['og_universe'] = "Universe";
$lang['last_update'] = 'Last update';
$lang['next_update'] = 'Next update (approx.)';
$lang['og_system'] = 'System';
$lang['og_location'] = 'Location';
$lang['og_planet'] = 'Planet';
$lang['og_moon'] = 'Moon';
$lang['og_player_status'] = 'Player (status)';
$lang['og_position'] = 'Position';
$lang['og_alliance'] = 'Alliance';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = '%t% ago';
//Events in the future (aproximation)
$lang['in_n_time'] = 'In %t%';

$lang['second'] = 'second';
$lang['seconds'] = 'seconds';
$lang['minute'] = 'minute';
$lang['minutes'] = 'minutes';
$lang['hour'] = 'hour';
$lang['hours'] = 'hours';
$lang['day'] = 'day';
$lang['days'] = 'days';
$lang['week'] = 'week';
$lang['weeks'] = 'weeks';
$lang['month'] = 'month';
$lang['months'] = 'months';
$lang['year'] = 'year';
$lang['years'] = 'years';

//server detail
$lang['og_name'] = 'Name';
$lang['og_language'] = 'Language';
$lang['og_timezone'] = 'Time zone';
$lang['og_ogame_version'] = 'Ogame version';
$lang['og_galaxies'] = 'Galaxies';
$lang['og_systems'] = 'Systems';
$lang['og_rapid_fire'] = 'Rapid fire';
$lang['og_debris_factor'] = 'Debris factor';
$lang['og_repair_factor'] = 'Repair factor';
$lang['og_newbie_protection_limit'] = 'Newbie protection';
$lang['og_newbie_protection_high'] = 'Fleet protection';
$lang['og_other_info'] = 'Other information';
$lang['og_points_limit'] = 'Up to %d points';

//alliances

$lang['og_homepage'] = 'Home page';
$lang['og_updated_on'] = 'Updated on';
$lang['og_score'] = 'Score';
$lang['statistics'] = 'Statistics';
$lang['view'] = 'View';
$lang['og_members'] = 'Members';
$lang['og_alliance_registration'] = 'Registration';
$lang['og_open'] = 'Open';
$lang['og_closed'] = 'Closed';

//Types of ranking information
$lang['og_total'] = 'Total';
$lang['og_economy'] = 'Economy';
$lang['og_research'] = 'Research';
$lang['og_mil_points'] = 'Military';
$lang['og_lost_mil_points'] = 'Military Lost';
$lang['og_built_mil_points'] = 'Military Built';
$lang['og_destroyed_mil_points'] = 'Military Destroyed';
$lang['og_honor'] = 'Honor';


//player
$lang['og_player'] = 'Player';
$lang['og_num_ships'] = '# of Ships';
//Player's planets Ogniter is aware of
$lang['og_known_planets'] = 'Known planets';
$lang['og_type'] = 'Type';
$lang['og_size'] = 'Size';

//ranking
$lang['og_results_by_alliance'] = 'Results by Alliance';
$lang['og_results_by_player'] = 'Results by Player';
$lang['og_alliance_tag'] = 'Tag';
$lang['og_average_score_per_player'] = 'Average score';

//search
$lang['og_search_by'] = 'Search by';
$lang['og_alliance_tag_long'] = 'Alliance tag';
//Search text
$lang['og_text'] = 'Text';
//Submit a form
$lang['og_send'] = 'Send';
$lang['og_please_do_a_db_search'] = 'Please enter your search';
$lang['og_search_results_by'] = 'Search results,';

//user status
$lang['og_inactive'] = 'Inactive';
$lang['og_inactive_30'] = 'Inactive (30 days)';
$lang['og_v_mode'] = 'Vacation mode';
$lang['og_suspended'] = 'Suspended';
$lang['og_outlaw'] = 'Outlaw';

//statistics
$lang['by_week'] = 'Week';
$lang['by_month'] = 'Month';
$lang['by_year'] = 'Year';
$lang['all'] = 'All';

//Compare statistics
$lang['og_comparison'] = 'Compare';
$lang['compare_statistics'] = 'Compare statistical evolution of players and alliances';
$lang['search_players'] = 'Search Players';
$lang['search_alliances'] = 'Search Alliances';

//Polls
$lang['vote'] = 'Vote';
$lang['view_results'] = 'View results';
$lang['poll_results'] = 'Poll results';
$lang['poll_vote_ok'] = 'Your vote has been cast successfully';
$lang['poll_vote_error'] = 'There was a problem when processing your vote';

//Colonize
$lang['og_colonize'] = 'Colonize';

//Flight times
$lang['time_calc'] = 'Flight time calculator';
$lang['og_flight_times'] = 'Flight Times';
$lang['start'] = 'Start';
$lang['destination'] = 'Destination';

$lang['og_updating'] = 'Updating...';
$lang['og_wait_a_few_seconds'] = 'Wait a few seconds, and then <a href="%s%">try again</a>';

$lang['flight_time_calculator'] = 'Flight Time Calculator';
$lang['required_field'] = 'The value is required';
$lang['integer_n_digits'] = 'Only integer values, up to %s% digits';
$lang['integer_only'] = 'Only integer values';
$lang['motors'] = 'Motors';
$lang['start_time'] = 'Start date';
$lang['fleet_ships'] = 'Ships';
$lang['uni_speed'] = 'Universe Speed';
$lang['fleet_speed'] = 'Fleet Speed';
$lang['calc_times'] = 'Calc total time';

$lang['result_time'] = 'Total flight time';
$lang['begin_hour'] = 'Start date';
$lang['arriving_time'] = 'Time of arrival';
$lang['end_hour'] = 'End date';

$lang['invalidDateTime'] = 'You must enter a valid date';
$lang['mustAddAShip'] = 'You must enter at least a ship';
$lang['tools'] = 'Tools';

//Resources & Technologies
$lang["small_cargo"] = 'Small Cargo';
$lang["large_cargo"] = 'Large Cargo';
$lang["light_fighter"] = 'Light Fighter';
$lang["heavy_fighter"] = 'Heavy Fighter';
$lang["cruiser"] = 'Cruiser';
$lang["battle_ship"] = 'Battleship';
$lang["colony_ship"] = 'Colony Ship';
$lang["recycler"] = 'Recycler';
$lang["esp_probe"] = 'Espionage Probe';
$lang["bomber_ship"] = 'Bomber';
$lang["solar_sat"] = 'Solar Satellite';
$lang["destroyer"] = 'Destroyer';
$lang["death_star"] = 'Deathstar';
$lang["battle_cruiser"] = 'Battlecruiser';
$lang["rocket_launcher"] = 'Rocket Launcher';
$lang["light_laser"] = 'Light Laser';
$lang["heavy_laser"] = 'Heavy Laser';
$lang["gauss_cannon"] = 'Gauss Cannon';
$lang["ion_cannon"] = 'Ion Cannon';
$lang["plasma_turret"] = 'Plasma Turret';
$lang["small_shield_dome"] = 'Small Shield Dome';
$lang["large_shield_dome"] = 'Large Shield Dome';

$lang["military_tech"] = 'Weapons Technology';
$lang["defense_tech"] = 'Shielding Technology';
$lang["hull_tech"] = 'Armor Technology';

$lang["combustion_drive_tech"] = 'Combustion Drive';
$lang["impulse_drive_tech"] = 'Impulse Drive';
$lang["hyperspace_drive_tech"] = 'Hyperspace Drive';
//

$lang['theme'] = 'Theme';
$lang['report_suggestions'] = 'Send your suggestions and comments to <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Universes from: ';
$lang['description_domain_module'] = 'Ogame universes list: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Ogame tools. Ogame flight time calculator';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, universe %server% - %domain% (statistics)';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Galaxy view [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Universe %server% (galaxy view) - %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Search Players, Planets &amp; Alliances (%domain%)';
$lang['description_server_search'] = 'Ogame, Universe %server% (search information page) - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Ranking (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Ranking scores from %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. %s% alliance data, %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Ranking information and player list of %s%, from %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. %s% player data in %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, Ranking information and planet list of %s%, from %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Compare Players or Alliances. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Comparison of Players &amp; Alliances from %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Statistical graphics, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, Statistics information, from %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Top %n% Players';
$lang['description_top_n_players'] = 'Ogniter. Top %n% Players ranking';

$lang['title_top_n_alliances'] = 'Ogniter. Top %n% Alliances';
$lang['description_top_n_alliances'] = 'Ogniter. Top %n% Alliances ranking';

$lang['pls_donate'] = 'Ogniter is a free website, with monthly payment costs above the average web-hosting package.<br />
            We are accepting now donations of users who wish to cooperate with the maintenance, and the development of new features on ogniter.org.<br /> Don\'t forget to include your name and email!';

$lang['share'] = 'Share';
$lang['planets'] = 'Planets';
$lang['moons'] = 'Moons';

$lang['discussions'] = 'Discussions';

$lang['find_free_slots'] = 'Find free positions';
$lang['occupied_planets'] = 'Occupied planets';
$lang['find_planets'] = 'Locate planets';
$lang['caption'] = 'Caption';
$lang['free_slots_range'] = 'From %d to %d occupied planets';
$lang['n_occupied_planets'] = '%d occupied planets';

$lang['new'] = 'New';
$lang['alliance_planets'] = 'Alliance\'s planets';
$lang['members_statistics'] = 'Members statistics';

$lang['description_server_track_alliance'] = 'Planet search of the alliance %s%, %server% - %domain%';
$lang['description_server_track_slots'] = 'Search for free positions in %server% - %domain%';
$lang['not_found_try_again'] = 'Not found. Please try again tomorrow.';

$lang['universe_limits'] = 'Limits of the universe';
$lang['category'] = 'Category';
$lang['thread'] = 'Thread';

$lang['galaxy_tools'] = 'Galaxy tools';
$lang['planet_search_by_status'] = 'Search planets by player\'s status';
$lang['player_status'] = "Player\'s status";
$lang['by_player_status'] = "Find by player's status";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Top Searches';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

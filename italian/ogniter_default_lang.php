<?php

//Web site title
$lang['og_site_title'] = "Ogniter - Banca dati on-line di Ogame";
//unused
$lang['og_logo_text'] = "ODB";

//"Domain", as a group of ogame related universes (ogame.fr, ogame.it, etc...)
$lang['og_domain'] = "Dominio";
$lang['og_domains'] = "Domini";

//You may translate this as "Server", or "Universe"
$lang['og_server'] = "Universo";
$lang['og_servers'] = "Universi";

$lang['og_online_users'] = "Utenti on-line";

//Default page description
$lang['og_description'] = 'Base di dati on-line di Ogame. Galassia, Posizione. Ogame strumenti gratuiti';

$lang['og_pick_a_domain'] = "Scegli un dominio";
$lang['og_choose_a_server'] = "Scegli un universo";
$lang['og_latest_servers'] = "Ultimi universi";
$lang['screenshots'] = 'Screenshots';

$lang['og_home_title'] = 'Ogniter, <small>banca dati on-line di Ogame.</small>';
$lang['og_home_description'] = 'Pianeti, le classifiche, i giocatori e informazioni alleanza di gioco.';
$lang['og_home_description_2'] = 'The best choice if you dont have a Galaxytool available.';
$lang['og_home_how_to_start'] = 'Come iniziare: <strong>scegli</strong> un dominio, e quindi <strong>scegli</strong> il tuo Universo favorito nella lista';
$lang['og_home_comeback'] = 'There\'s always something new. Enjoy your stay and come back later.';

$lang['og_home_domain_list'] = 'Domini';
//Available universes
$lang['og_number_of_servers'] = 'Numero di universi registrati';

$lang['og_news'] = 'Notizie';

$lang['og_terms_of_use'] = 'Condizioni di utilizzo';
$lang['og_privacy_policy'] = 'Informativa sulla privacy';
$lang['og_copyright'] = 'Copyright';
$lang['og_developed_by'] = 'Ogniter, developed and maintained by';
$lang['og_game_created_by'] = 'Ogame is a multiplayer online game, created by Gameforge';

$lang['og_home'] = 'Home';

$lang['top_n_players'] = 'Top %n% giocatori';
$lang['top_n_alliances'] = 'Top %n% Alleanze';

$lang['ogame_top_n_players'] = 'Ogame Top %n% giocatori';
$lang['ogame_top_n_alliances'] = 'Ogame Top %n% Alleanze';

//Position of the player or alliance in their universe
$lang['uni_position'] = 'Uni. posizione';

//list of universes
$lang['og_server_list'] = 'Elenco Universo';
$lang['og_server_list_from'] = 'Elenco Universo nei %s%';
$lang['og_acs'] = 'ACS';
$lang['og_speed'] = 'Velocità';
$lang['og_def_to_debris'] = 'Difesa a detriti';
$lang['og_num_players'] = 'Num. di giocatori';
$lang['og_num_alliances'] = 'Num. di alleanze';
$lang['og_max_score'] = 'Punteggio massimo';
$lang['og_enabled'] = 'Abilitato';
$lang['og_disabled'] = 'Disabile';

$lang['og_server_info'] = 'Informazioni dell\'Universo';
$lang['og_search'] = 'Cercare';
$lang['og_galaxy'] = 'Galassia';
$lang['og_ranking'] = 'Posizione';

//Unused
$lang['og_player_statuses'] = 'Player\'s database';

//galaxy
$lang['og_galaxy_view'] = "Vista galassia";
$lang['og_weekly_increment'] = 'Incremento <br />settimanale';
$lang['og_monthly_increment'] = 'Incremento <br />mensile';
$lang['og_universe'] = "Universo";
$lang['last_update'] = 'Ultimo aggiornamento';
$lang['next_update'] = 'Prossimo aggiornamento (approssimativo)';
$lang['og_system'] = 'Sistema';
$lang['og_location'] = 'Localizzazione';
$lang['og_planet'] = 'Pianeta';
$lang['og_moon'] = 'Luna';
$lang['og_player_status'] = 'Giocatore (stato)';
$lang['og_position'] = 'Posizione';
$lang['og_alliance'] = 'Alleanza';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = '%t% fa';
//Events in the future (aproximation)
$lang['in_n_time'] = 'In %t%';

$lang['second'] = 'secondo';
$lang['seconds'] = 'secondi';
$lang['minute'] = 'minuto';
$lang['minutes'] = 'minuti';
$lang['hour'] = 'ora';
$lang['hours'] = 'ore';
$lang['day'] = 'giorno';
$lang['days'] = 'giorni';
$lang['week'] = 'settimana';
$lang['weeks'] = 'settimane';
$lang['month'] = 'mese';
$lang['months'] = 'mesi';
$lang['year'] = 'anno';
$lang['years'] = 'anni';

//server detail
$lang['og_name'] = 'Nome';
$lang['og_language'] = 'Lingua';
$lang['og_timezone'] = 'Fuso orario';
$lang['og_ogame_version'] = 'Versione di Ogame';
$lang['og_galaxies'] = 'Galassie';
$lang['og_systems'] = 'Sistemi';
$lang['og_rapid_fire'] = 'Fuoco Rapido';
$lang['og_debris_factor'] = 'Fattore di detriti';
$lang['og_repair_factor'] = 'Fattore de riparazione';
$lang['og_newbie_protection_limit'] = 'Protezione di giocatore';
$lang['og_newbie_protection_high'] = 'Protezione di flotta';
$lang['og_other_info'] = 'Altre informazioni';
$lang['og_points_limit'] = 'Fino a %d punti';

//alliances
$lang['og_homepage'] = 'Home page';
$lang['og_updated_on'] = 'Aggiornato il';
$lang['og_score'] = 'Punteggio';
$lang['statistics'] = 'Statistics';
$lang['view'] = 'Statistica';
$lang['og_members'] = 'Membri';
$lang['og_alliance_registration'] = 'Iscrizioni';
$lang['og_open'] = 'Aperte';
$lang['og_closed'] = 'Chiuso';

//Types of ranking information
$lang['og_total'] = 'Totale';
$lang['og_economy'] = 'Economia';
$lang['og_research'] = 'Ricerca';
$lang['og_mil_points'] = 'Militare';
$lang['og_lost_mil_points'] = 'P. m. persi';
$lang['og_built_mil_points'] = 'P. m. costruiti';
$lang['og_destroyed_mil_points'] = 'P. m. distrutti';
$lang['og_honor'] = 'Onore';


//player
$lang['og_player'] = 'Giocatore';
$lang['og_num_ships'] = 'Num. di navi';
//Player's planets Ogniter is aware of
$lang['og_known_planets'] = 'Pianeti noti';
$lang['og_type'] = 'Tipo';
$lang['og_size'] = 'Dimenzione';

//ranking
$lang['og_results_by_alliance'] = 'Risultati per alleanza';
$lang['og_results_by_player'] = 'Risultati per giocatore';
$lang['og_alliance_tag'] = 'Etichetta';
$lang['og_average_score_per_player'] = 'Valutazione media';

//search
$lang['og_search_by'] = 'Ricerca per';
$lang['og_alliance_tag_long'] = 'Etichetta dell\'Alleanza';
//Search text
$lang['og_text'] = 'Testo';
//Submit a form
$lang['og_send'] = 'Inviare';
$lang['og_please_do_a_db_search'] = 'Per favore, inserisci una ricerca';
$lang['og_search_results_by'] = 'Risultati della ricerca per';

//user status
$lang['og_inactive'] = 'Inattivo';
$lang['og_inactive_30'] = 'Inattivo (30 giorni)';
$lang['og_v_mode'] = 'Modalità vacanza';
$lang['og_suspended'] = 'Sospeso';
$lang['og_outlaw'] = 'Fuorilegge';

//statistics
$lang['by_week'] = 'Settimana';
$lang['by_month'] = 'Mese';
$lang['by_year'] = 'Anno';
$lang['all'] = 'Tutti';

//Compare statistics
$lang['og_comparison'] = 'Confrontare';
$lang['compare_statistics'] = 'Confronta evoluzione statistiche dei giocatori e alleanze';
$lang['search_players'] = 'Ricerca giocatori';
$lang['search_alliances'] = 'Ricerca alleanze';

//Polls
$lang['vote'] = 'Votare';
$lang['view_results'] = 'Visualizza risultati';
$lang['poll_results'] = 'Risultati del sondaggio';
$lang['poll_vote_ok'] = 'Il tuo voto è stato lanciato con successo';
$lang['poll_vote_error'] = 'C\'è stato un problema durante l\'elaborazione il tuo voto';

//Colonize
$lang['og_colonize'] = 'Colonizzare';

//Flight times
$lang['time_calc'] = 'Flight time calculator';
$lang['og_flight_times'] = 'Flight Times';
$lang['start'] = 'Start';
$lang['destination'] = 'Destination';

$lang['og_updating'] = 'Updating...';
$lang['og_wait_a_few_seconds'] = 'Wait a few seconds, and then <a href="%s%">try again</a>';

$lang['flight_time_calculator'] = 'Tempo di volo calcolatrice';
$lang['required_field'] = 'Questo è necessario';
$lang['integer_n_digits'] = 'Solo i valori interi, fino a %s% cifre';
$lang['integer_only'] = 'Solo i valori interi';
$lang['motors'] = 'Motori';
$lang['start_time'] = 'Data di inizio';
$lang['fleet_ships'] = 'Navi';
$lang['uni_speed'] = "Velocità dell'Universo";
$lang['fleet_speed'] = 'Velocità di flotta';
$lang['calc_times'] = 'Calcolare il tempo totale';

$lang['result_time'] = 'Tempo di volo totale';
$lang['begin_hour'] = 'Data di inizio';
$lang['arriving_time'] = 'Ora di arrivo';
$lang['end_hour'] = 'Data di fine';

$lang['invalidDateTime'] = 'È necessario inserire una data valida';
$lang['mustAddAShip'] = 'Devi inserire almeno una nave';
$lang['tools'] = 'Strumenti';

//Resources & Technologies
//Ogame API has already a translation for these. For example:
//http://uni101.ogame.fr/api/techNames.xml
$lang["small_cargo"] = 'Cargo leggero';
$lang["large_cargo"] = 'Cargo Pesante';
$lang["light_fighter"] = 'Caccia Leggero';
$lang["heavy_fighter"] = 'Caccia Pesante';
$lang["cruiser"] = 'Incrociatore';
$lang["battle_ship"] = 'Nave da battaglia';
$lang["colony_ship"] = 'Colonizzatrice';
$lang["recycler"] = 'Riciclatrice';
$lang["esp_probe"] = 'Sonda spia';
$lang["bomber_ship"] = 'Bombardiere';
$lang["solar_sat"] = 'Satellite Solare';
$lang["destroyer"] = 'Corazzata';
$lang["death_star"] = 'Morte Nera';
$lang["battle_cruiser"] = 'Incrociatore da Battaglia';
$lang["rocket_launcher"] = 'Lanciamissili';
$lang["light_laser"] = 'Laser leggero';
$lang["heavy_laser"] = 'Laser pesante';
$lang["gauss_cannon"] = 'Cannone Gauss';
$lang["ion_cannon"] = 'Cannone ionico';
$lang["plasma_turret"] = 'Cannone al Plasma';
$lang["small_shield_dome"] = 'Cupola scudo piccola';
$lang["large_shield_dome"] = 'Cupola scudo potenziata';

$lang["military_tech"] = 'Tecnologia delle armi';
$lang["defense_tech"] = 'Tecnologia degli scudi';
$lang["hull_tech"] = 'Tecnologia delle corazze';

$lang["combustion_drive_tech"] = 'Propulsore a combustione';
$lang["impulse_drive_tech"] = 'Propulsore a impulso';
$lang["hyperspace_drive_tech"] = 'Propulsore iperspaziale';
//

$lang['theme'] = 'Disegno';
$lang['report_suggestions'] = 'Send your suggestions and comments to <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Universi da: ';
$lang['description_domain_module'] = 'Ogame universes lista: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Ogame strumenti. Tempo di volo calcolatrice (Ogame)';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, universo %server% - %domain% (statistics)';

$lang['title_server_galaxy'] = 'Ogniter. %server%: Visione della galassia [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Universo %server% (visione della galassia) - %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Cerca Giocatori, Pianeti & Alliances (%domain%)';
$lang['description_server_search'] = 'Ogame, Universo %server% (ricerca di informazioni) - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server%: Posizione (%domain%)';
$lang['description_server_ranking'] = 'Ogame, classifica punteggi da %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. %s%  alleanza dati, %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Classifica informazioni e lista dei giocatori di %s%, in %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. %s% giocatore dati, %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, Classifica informazioni e lista pianeta di %s%, in %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Confronta i giocatori o alleanze. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Confronto tra giocatori e alleanze in %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. Grafici statistici, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, Statistiche informazioni, da %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Primi %n% Giocatori';
$lang['description_top_n_players'] = 'Ogniter. Primi %n% Giocatori';

$lang['title_top_n_alliances'] = 'Ogniter. Prime %n% Alleanze';
$lang['description_top_n_alliances'] = 'Ogniter. Prime %n% Alleanze';

$lang['pls_donate'] = 'Ogniter is a free access website, with monthly payment costs above the average web-hosting package.<br />
            We are accepting now donations of users who wish to cooperate with the maintenance, and the development of new features on ogniter.org.<br /> Don\'t forget to include your name and email!';

$lang['share'] = 'Spartire';
$lang['planets'] = 'Pianeti';
$lang['moons'] = 'Lune';

$lang['discussions'] = 'Discussioni';

$lang['find_free_slots'] = 'Trova posizioni libere';
$lang['occupied_planets'] = 'Pianeti occupati';
$lang['find_planets'] = 'Trovare pianeti';
$lang['caption'] = 'Didascalia';
$lang['free_slots_range'] = 'Da %d a %d pianeti occupati';
$lang['n_occupied_planets'] = '%d pianeti occupati';

$lang['new'] = 'Nuovo';
$lang['alliance_planets'] = 'Pianeti dell\'alleanza';
$lang['members_statistics'] = 'Statistiche dei membri';

$lang['description_server_track_alliance'] = 'Ricerca pianeta delle %s% alleanza, %server% - %domain%';
$lang['description_server_track_slots'] = 'Ricerca di posizioni libere a %server% - %domain%';
$lang['not_found_try_again'] = 'Non trovato. Si prega di riprovare domani.';

$lang['universe_limits'] = 'Limiti dell\'universo';
$lang['category'] = 'Categoria';
$lang['thread'] = 'Tema';

$lang['galaxy_tools'] = 'Galaxy strumenti';
$lang['planet_search_by_status'] = 'Pianeti Ricerca per stato del giocatore';
$lang['player_status'] = "Stato del giocatore";
$lang['by_player_status'] = "Ricerca per stato del giocatore";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Ricerche';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metallo';
$lang['crystal'] = 'Cristallo';
$lang['deuterium'] = 'Deuterio';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

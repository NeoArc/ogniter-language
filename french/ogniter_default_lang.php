<?php

//By Florent Lanternier

//Web site title
$lang['og_site_title'] = 'Ogniter - Base de données d\'OGame';
//unused
$lang['og_logo_text'] = "ODB";

//"Domain", as a group of ogame related universes (ogame.fr, ogame.it, etc...)
$lang['og_domain'] = "Serveur";
$lang['og_domains'] = "Serveurs";

//You may translate this as "Server", or "Universe"
$lang['og_server'] = "Univers";
$lang['og_servers'] = "Univers";

$lang['og_online_users'] = "En ligne";

//Default page description
$lang['og_description'] = 'Base de données gratuites d\'OGame. Statistiques, cartographie &amp; outils gratuits pour OGame';

$lang['og_pick_a_domain'] = "Sélectionnez un serveur";
$lang['og_choose_a_server'] = "Choisissez un univers";
$lang['og_latest_servers'] = "Derniers univers";
$lang['screenshots'] = 'Captures d\'écrans';

$lang['og_home_title'] = 'Ogniter, <small>base de données d\'OGame en ligne</small>';
$lang['og_home_description'] = 'Toutes les galaxie, classements, joueurs &amp; alliances';
$lang['og_home_description_2'] = 'Le meilleur outils disponible si vous ne pouvez pas créer ou maintenir une cartographie privée.';
$lang['og_home_how_to_start'] = 'Pour commencer : <strong>sélectionnez</strong> un serveur, puis <strong>choisissez</strong> votre univers favori dans la liste.';
$lang['og_home_comeback'] = 'Il y a tout le temps de nouvelles choses mises en place. Profitez, et revenez nous voir.';

$lang['og_home_domain_list'] = 'Liste des serveurs';
//Available universes
$lang['og_number_of_servers'] = '# d\'univers répertoriés';

$lang['og_news'] = 'Quoi de neuf ?';

$lang['og_terms_of_use'] = 'Conditions d\'utilisation';
$lang['og_privacy_policy'] = 'Politique de confidentialité';
$lang['og_copyright'] = 'Copyright';
$lang['og_developed_by'] = 'Ogniter, développé et maintenu par';
$lang['og_game_created_by'] = 'OGame est un jeu multijoueur par navigateur, créé par Gameforge GmbH';

$lang['og_home'] = 'Accueil';

$lang['top_n_players'] = 'Top %n% des joueurs';
$lang['top_n_alliances'] = 'Top %n% des alliances';

$lang['ogame_top_n_players'] = 'Top %n% des joueurs d\'OGame';
$lang['ogame_top_n_alliances'] = 'Top %n% des alliances d\'OGame';

//Position of the player or alliance in their universe
$lang['uni_position'] = 'Classement dans l\'univers';

//list of universes
$lang['og_server_list'] = 'Liste des univers';
$lang['og_server_list_from'] = 'Liste des univers d\'%s%';
$lang['og_acs'] = 'AG';
$lang['og_speed'] = 'Vitesse';
$lang['og_def_to_debris'] = 'Défenses dans le CDR';
$lang['og_num_players'] = '# de joueurs';
$lang['og_num_alliances'] = '# d\'alliances';
$lang['og_max_score'] = 'Nombre de points max';
$lang['og_enabled'] = 'Activé';
$lang['og_disabled'] = 'Désactivé';

$lang['og_server_info'] = 'Détails de l\'univers';
$lang['og_search'] = 'Rechercher';
$lang['og_galaxy'] = 'Galaxie';
$lang['og_ranking'] = 'Classement';

//Unused
$lang['og_player_statuses'] = 'Player\'s database';

//galaxy
$lang['og_galaxy_view'] = "Afficher la galaxie";
$lang['og_weekly_increment'] = 'Progression <br />hebdomadaire';
$lang['og_monthly_increment'] = 'Progression <br />mensuelle';
$lang['og_universe'] = "Univers";
$lang['last_update'] = 'Dernière mise à jour';
$lang['next_update'] = 'Prochaine mise à jour (approx.)';
$lang['og_system'] = 'Système';
$lang['og_location'] = 'Position';
$lang['og_planet'] = 'Planète';
$lang['og_moon'] = 'Lune';
$lang['og_player_status'] = 'Joueur (statut)';
$lang['og_position'] = 'Position';
$lang['og_alliance'] = 'Alliance';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = 'Il y a %t%';
//Events in the future (aproximation)
$lang['in_n_time'] = 'Dans %t%';

$lang['second'] = 'seconde';
$lang['seconds'] = 'secondes';
$lang['minute'] = 'minute';
$lang['minutes'] = 'minutes';
$lang['hour'] = 'heure';
$lang['hours'] = 'heures';
$lang['day'] = 'jour';
$lang['days'] = 'jours';
$lang['week'] = 'semaine';
$lang['weeks'] = 'semaines';
$lang['month'] = 'mois';
$lang['months'] = 'mois';
$lang['year'] = 'année';
$lang['years'] = 'années';

//server detail
$lang['og_name'] = 'Nom';
$lang['og_language'] = 'Langue';
$lang['og_timezone'] = 'Fuseau horaire';
$lang['og_ogame_version'] = 'Version d\'OGame';
$lang['og_galaxies'] = 'Galaxies';
$lang['og_systems'] = 'Systèmes';
$lang['og_rapid_fire'] = 'Rapid Fire';
$lang['og_debris_factor'] = 'Champs de débris';
$lang['og_repair_factor'] = 'Réparation de la défense';
$lang['og_newbie_protection_limit'] = 'Protection des jeunes joueurs';
$lang['og_newbie_protection_high'] = 'Protection de la flotte';
$lang['og_other_info'] = 'Autres informations';
$lang['og_points_limit'] = 'Jusqu\'à %d points';

//alliances

$lang['og_homepage'] = 'Page d\'accueil';
$lang['og_updated_on'] = 'Mis à jour le';
$lang['og_score'] = 'Score';
$lang['statistics'] = 'Statistiques';
$lang['view'] = 'Vue';
$lang['og_members'] = 'Membres';
$lang['og_alliance_registration'] = 'Inscription';
$lang['og_open'] = 'Ouvert';
$lang['og_closed'] = 'Fermé';

//Types of ranking information
$lang['og_total'] = 'Total';
$lang['og_economy'] = 'Economie';
$lang['og_research'] = 'Recherches';
$lang['og_mil_points'] = 'Militaires';
$lang['og_lost_mil_points'] = 'M. perdus';
$lang['og_built_mil_points'] = 'M. construits';
$lang['og_destroyed_mil_points'] = 'M. détruits';
$lang['og_honor'] = 'Honorifiques';


//player
$lang['og_player'] = 'Joueur';
$lang['og_num_ships'] = '# de vaisseaux';
//Player's planets Ogniter is aware of
$lang['og_known_planets'] = 'Planètes connues';
$lang['og_type'] = 'Type';
$lang['og_size'] = 'Taille';

//ranking
$lang['og_results_by_alliance'] = 'Résultats par Alliance';
$lang['og_results_by_player'] = 'Résultats par Joueur';
$lang['og_alliance_tag'] = 'Tag';
$lang['og_average_score_per_player'] = 'Moyenne de points';

//search
$lang['og_search_by'] = 'Rechercher par';
$lang['og_alliance_tag_long'] = 'Tag d\'alliance';
//Search text
$lang['og_text'] = 'Texte';
//Submit a form
$lang['og_send'] = 'Envoyer';
$lang['og_please_do_a_db_search'] = 'Entrez une recherche s\'il vous plait';
$lang['og_search_results_by'] = 'Résultats de la recherche,';

//user status
$lang['og_inactive'] = 'Inactif';
$lang['og_inactive_30'] = 'Inactif (30 jours)';
$lang['og_v_mode'] = 'Mode vacance';
$lang['og_suspended'] = 'Bloqué';
$lang['og_outlaw'] = 'Hors-la-loi';

//statistics
$lang['by_week'] = 'Semaine';
$lang['by_month'] = 'Mois';
$lang['by_year'] = 'Année';
$lang['all'] = 'Tout';

//Compare statistics
$lang['og_comparison'] = 'Comparer';
//$lang['og_comparison'] = 'Comparer les progressions';
$lang['compare_statistics'] = 'Comparer l\'évolution des statistiques des joueurs et des alliances';
$lang['search_players'] = 'Rechercher des joueurs';
$lang['search_alliances'] = 'Rechercher des alliances';

//Polls
$lang['vote'] = 'Voter';
$lang['view_results'] = 'Voir les résultats';
$lang['poll_results'] = 'Résultats du vote';
$lang['poll_vote_ok'] = 'Votre vote a bien été pris en compte';
$lang['poll_vote_error'] = 'Il y a eu un problème lors de votre vote';

//Colonize
$lang['og_colonize'] = 'Coloniser';

//Flight times
$lang['time_calc'] = 'Calculateur de temps de vol';
$lang['og_flight_times'] = 'Temps de vol';
$lang['start'] = 'Départ';
$lang['destination'] = 'Arrivée';

$lang['og_updating'] = 'Mise à jour...';
$lang['og_wait_a_few_seconds'] = 'Attendez quelques secondes, puis <a href="%s%">réessayer</a>';

$lang['flight_time_calculator'] = 'Calculateur de temsp de vol';
$lang['required_field'] = 'La valeur est requise';
$lang['integer_n_digits'] = 'Seulement des valeurs entières, jusqu\'à %s% chiffres';
$lang['integer_only'] = 'Seulement des valeurs entières';
$lang['motors'] = 'Moteurs';
$lang['start_time'] = 'Heure de début';
$lang['fleet_ships'] = 'Vaisseaux';
$lang['uni_speed'] = 'Vitesse de l\'univers';
$lang['fleet_speed'] = 'Vitesse de la flotte';
$lang['calc_times'] = 'Calculer le temps total';

$lang['result_time'] = 'Durée totale du vol';
$lang['begin_hour'] = 'Heure de début';
$lang['arriving_time'] = 'Heure d\'arrivée';
$lang['end_hour'] = 'Heure de retour';

$lang['invalidDateTime'] = 'Vous devez entrer une date valide';
$lang['mustAddAShip'] = 'Vous devez entrer au moins un vaisseau';
$lang['tools'] = 'Outils';

//Resources & Technologies
$lang["small_cargo"] = 'Petit transporteur';
$lang["large_cargo"] = 'Grand transporteur';
$lang["light_fighter"] = 'Chasseur léger';
$lang["heavy_fighter"] = 'Chasseur lourd';
$lang["cruiser"] = 'Croiseur';
$lang["battle_ship"] = 'Vaisseau de bataille';
$lang["colony_ship"] = 'Vaisseau de colonisation';
$lang["recycler"] = 'Recycleur';
$lang["esp_probe"] = 'Sonde d`espionnage';
$lang["bomber_ship"] = 'Bombardier';
$lang["solar_sat"] = 'Satellite solaire';
$lang["destroyer"] = 'Destructeur';
$lang["death_star"] = 'Étoile de la mort';
$lang["battle_cruiser"] = 'Traqueur';
$lang["rocket_launcher"] = 'Lanceur de missiles';
$lang["light_laser"] = 'Artillerie laser légère';
$lang["heavy_laser"] = 'Artillerie laser lourde';
$lang["gauss_cannon"] = 'Canon de Gauss';
$lang["ion_cannon"] = 'Artillerie à ions';
$lang["plasma_turret"] = 'Lanceur de plasma';
$lang["small_shield_dome"] = 'Petit bouclier';
$lang["large_shield_dome"] = 'Grand bouclier';

$lang["military_tech"] = 'Technologie Armes';
$lang["defense_tech"] = 'Technologie Bouclier';
$lang["hull_tech"] = 'Technologie Protection des vaisseaux spatiaux';

$lang["combustion_drive_tech"] = 'Réacteur à combustion';
$lang["impulse_drive_tech"] = 'Réacteur à impulsion';
$lang["hyperspace_drive_tech"] = 'Propulsion hyperespace';
//

$lang['theme'] = 'Style';
$lang['report_suggestions'] = 'Envoyez vos suggestions et commentaires à <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Les univers de : ';
$lang['description_domain_module'] = 'Liste des univers d\'OGame: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, outisl pour OGame. Calculateur de temsp de vol pour OGame';

$lang['title_server_index'] = 'Ogniter. %server% (%domain%)';
$lang['description_server_index'] = 'Ogame, univers  %server% - %domain% (statistiques)';

$lang['title_server_galaxy'] = 'Ogniter. %server% : Vue de la galaxie [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Univers  %server% (vue de la galaxie) - %domain%';

$lang['title_server_search'] = 'Ogniter. %server% : Rechercher des joueurs, planètes &amp; alliances (%domain%)';
$lang['description_server_search'] = 'Ogame, Univers %server% (page de recherche) - %domain%';

$lang['title_server_ranking'] = 'Ogniter. %server% : Classement (%domain%)';
$lang['description_server_ranking'] = 'Ogame, Classement de %server% - %domain%';

$lang['title_server_alliance'] = 'Ogniter. %s% données de l\'alliance, %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Informations sur le classement et liste des joueurs de %s%, sur %server% - %domain%';

$lang['title_server_player'] = 'Ogniter. %s% données du joueur sur %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, Informations sur le classement et liste des planètes de %s%, sur %server% - %domain%';

$lang['title_server_comparison'] = 'Ogniter. Comparer les joueurs et alliances. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, Comparaison des joueurs &amp; alliances sur %server% - %domain%';

$lang['title_server_statistics'] = 'Ogniter. graphiques statistiques, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, Statistiques, sur %server% - %domain%';

$lang['title_top_n_players'] = 'Ogniter. Top %n% Joueurs';
$lang['description_top_n_players'] = 'Ogniter. Top %n% classement joueurs';

$lang['title_top_n_alliances'] = 'Ogniter. Top %n% Alliances';
$lang['description_top_n_alliances'] = 'Ogniter. Top %n% classement alliances';

$lang['pls_donate'] = 'Ogniter est un site internet gratuit, qui nécessite des frais d\'hébergement supérieurs à la moyenne.<br />
            Nous acceptons dès à présent les dons d\'utilisateurs qui souhaitent coopérer pour le maintien et le développement de nouvelles fonctionnalités sur ogniter.org.<br /> N\'oubliez pas d\'inclure votre nom et votre email !';

$lang['share'] = 'Partager';
$lang['planets'] = 'Planètes';
$lang['moons'] = 'Lunes';

$lang['discussions'] = 'Discussions';

$lang['find_free_slots'] = 'Trouver des positions libres';
$lang['occupied_planets'] = 'Planètes occupées';
$lang['find_planets'] = 'Trouver des planètes';
$lang['caption'] = 'Légende';
$lang['free_slots_range'] = 'De %d à %d planètes occupées';
$lang['n_occupied_planets'] = '%d planètes occupées';

$lang['new'] = 'Nouveau';
$lang['alliance_planets'] = 'Planètes de l\'alliance';
$lang['members_statistics'] = 'Statistiques des membres';

$lang['description_server_track_alliance'] = 'Rechercher les planètes de l\'alliance %s%, %server% - %domain%';
$lang['description_server_track_slots'] = 'Rechercher des positions libres sur %server% - %domain%';
$lang['not_found_try_again'] = 'Pas trouvé. Réessayer demain s\'il vous plait.';

$lang['universe_limits'] = 'Les limites de l\'univers';
$lang['category'] = 'Catégorie';
$lang['thread'] = 'Sujet';

$lang['galaxy_tools'] = 'Outils Galaxie';
$lang['planet_search_by_status'] = 'Recherche de planètes selon le statut des joueurs';
$lang['player_status'] = "Statuts des joueurs";
$lang['by_player_status'] = "Trouvez selon le statut du joueur";
$lang['normal'] = 'Normal';
$lang['popular_searches'] = 'Recherches';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';

<?php
//By Constantino Zhuikov

//Web site title
$lang['og_site_title'] = "Ogniter - База данных Ogame ";
//unused
$lang['og_logo_text'] = "ODB";

//"Domain", as a group of ogame related universes (ogame.fr, ogame.it, etc...)
$lang['og_domain'] = "Сервер";
$lang['og_domains'] = "Серверы";

//You may translate this as "Server", or "Universe"
$lang['og_server'] = "Вселенная";
$lang['og_servers'] = "Вселенных";

$lang['og_online_users'] = "Игроков онлайн";

//Default page description
$lang['og_description'] = 'Бесплатная база данных игроков MMORPG Ogame. Бесплатные инструменты.';

$lang['og_pick_a_domain'] = "Выберите сервер";
$lang['og_choose_a_server'] = "Выберите вселенную";
$lang['og_latest_servers'] = "Последние вселенные";
$lang['screenshots'] = 'Скриншоты';

$lang['og_home_title'] = 'Ogniter, <small>oнлайн база данных Ogame</small>';
$lang['og_home_description'] = 'Статистика серверов, вселенных, альянсов, игроков браузерной игры Ogame.';
$lang['og_home_description_2'] = 'Наилучший выбор для Вас, если у Вас нет Galaxy.';
$lang['og_home_how_to_start'] = 'Как начать: <strong>выберите</strong> сервер, а затем <strong>выберите</strong> нужную Вам вселенную в списке сервера.';
$lang['og_home_comeback'] = 'У нас всегда свежие данные. Пользуйтесь на здоровье и приходите еще.';

$lang['og_home_domain_list'] = 'Список серверов';
//Available universes
$lang['og_number_of_servers'] = '# зарегестрированных вселенных';

$lang['og_news'] = 'Что нового?';

$lang['og_terms_of_use'] = 'Условия использования';
$lang['og_privacy_policy'] = 'Политика конфиденциальности';
$lang['og_copyright'] = 'Авторские права';
$lang['og_developed_by'] = 'Ogniter, разработано и обслуживается';
$lang['og_game_created_by'] = 'Ogame многопользовательская игра, разработанная Gameforge';

$lang['og_home'] = 'Главная';

$lang['top_n_players'] = 'Топ %n% Игроков';
$lang['top_n_alliances'] = 'Топ %n% Альянсов';

$lang['ogame_top_n_players'] = 'Топ %n% игроков Огейм';
$lang['ogame_top_n_alliances'] = 'Топ %n% Альянсов Огейм';

//Position of the player or alliance in their universe
$lang['uni_position'] = 'Статистика вселенной';

//list of universes
$lang['og_server_list'] = 'Список вселенной';
$lang['og_server_list_from'] = 'Список вселенных на %s%'; 
$lang['og_acs'] = 'САБ';
$lang['og_speed'] = 'Скорость';
$lang['og_def_to_debris'] = 'ОВО';
$lang['og_num_players'] = '# из игроков';
$lang['og_num_alliances'] = '# из альянсов';
$lang['og_max_score'] = 'Макс. количество очков';
$lang['og_enabled'] = 'Доступно';
$lang['og_disabled'] = 'Недоступно';

$lang['og_server_info'] = 'Информация о вселенной';
$lang['og_search'] = 'Поиск';
$lang['og_galaxy'] = 'Галактика';
$lang['og_ranking'] = 'Статистика';

//Unused
$lang['og_player_statuses'] = 'Player\'s database';

//galaxy
$lang['og_galaxy_view'] = "Обзор галактики";
$lang['og_weekly_increment'] = 'Еженедельное<br />развитие';
$lang['og_monthly_increment'] = 'Ежемесячное<br />развитие';
$lang['og_universe'] = "Вселенная";
$lang['last_update'] = 'Последнее обновление';
$lang['next_update'] = 'Следующее обновление через(приблиз.)';
$lang['og_system'] = 'Система';
$lang['og_location'] = 'Адрес';
$lang['og_planet'] = 'Планета';
$lang['og_moon'] = 'Система';
$lang['og_player_status'] = 'Игрок(статус)';
$lang['og_position'] = 'Позиция';
$lang['og_alliance'] = 'Альянс';

//Events in the past (see galaxy page as a reference)
$lang['n_time_ago'] = '%t назад';
//Events in the future (aproximation)
$lang['in_n_time'] = 'Через %t% (приблиз.)';

$lang['minute'] = 'Минута';
$lang['minutes'] = 'Минут';
$lang['hour'] = 'Час';
$lang['hours'] = 'Часов';
$lang['day'] = 'День';
$lang['days'] = 'Дней';
$lang['week'] = 'Неделя';
$lang['weeks'] = 'Недели';
$lang['month'] = 'Месяц';
$lang['months'] = 'Месяцев';
$lang['year'] = 'Год';
$lang['years'] = 'Лет';

//server detail
$lang['og_name'] = 'Имя';
$lang['og_language'] = 'Язык';
$lang['og_timezone'] = 'Временная зона';
$lang['og_ogame_version'] = 'Версия Ogame';
$lang['og_galaxies'] = 'Галактик';
$lang['og_systems'] = 'Систем';
$lang['og_rapid_fire'] = 'Скорострел';
$lang['og_debris_factor'] = 'Поле обломков';
$lang['og_repair_factor'] = 'Процент восстановления ';
$lang['og_newbie_protection_limit'] = 'Защита новичков';
$lang['og_newbie_protection_high'] = 'Защита флота';
$lang['og_other_info'] = 'Доп. информация';
$lang['og_points_limit'] = 'До %d очков';

//alliances

$lang['og_homepage'] = 'Домашняя страница';
$lang['og_updated_on'] = 'Обновлено ';
$lang['og_score'] = 'Очки';
$lang['statistics'] = 'Статистика';
$lang['view'] = 'Обзор';
$lang['og_members'] = 'Список членов';
$lang['og_alliance_registration'] = 'Регистрация';
$lang['og_open'] = 'Открыт';
$lang['og_closed'] = 'Закрыт';

//Types of ranking information
$lang['og_total'] = 'Всего';
$lang['og_economy'] = 'Экономика';
$lang['og_research'] = 'Исследования';
$lang['og_mil_points'] = 'Вооружение';
$lang['og_lost_mil_points'] = 'Боевых очков потеряно';
$lang['og_built_mil_points'] = 'Боевых очков набрано';
$lang['og_destroyed_mil_points'] = 'Боевых очков уничтожено';
$lang['og_honor'] = 'очки чести';


//player
$lang['og_player'] = 'Игрок';
$lang['og_num_ships'] = 'Единиц флота:';
//Player's planets Ogniter is aware of
$lang['og_known_planets'] = 'Известные планеты';
$lang['og_type'] = 'Вид';
$lang['og_size'] = 'Размер';

//ranking
$lang['og_results_by_alliance'] = 'Результаты по альянсу';
$lang['og_results_by_player'] = 'Результаты по игроку';
$lang['og_alliance_tag'] = 'Тэг';
$lang['og_average_score_per_player'] = 'Среднее количество очков';

//search
$lang['og_search_by'] = 'Искать по';
$lang['og_alliance_tag_long'] = 'Аббревиатура альянса';
//Search text
$lang['og_text'] = 'Поиск...';
//Submit a form
$lang['og_send'] = 'Поиск';
$lang['og_please_do_a_db_search'] = 'Пожалуйста, заполните поле для поиска';
$lang['og_search_results_by'] = 'Результаты поиска';

//user status
$lang['og_inactive'] = 'Ишка';
$lang['og_inactive_30'] = 'Ишка (30 дней)';
$lang['og_v_mode'] = 'В отпуске';
$lang['og_suspended'] = 'Заблокирован'; 
$lang['og_outlaw'] = 'Вне закона';

//statistics
$lang['by_week'] = 'Недельная';
$lang['by_month'] = 'Месячная';
$lang['by_year'] = 'Годичная';
$lang['all'] = 'Всего';

//Compare statistics
$lang['og_comparison'] = 'Сравнить';
$lang['compare_statistics'] = 'Сравнить развитие игроков и альянсов'; 
$lang['search_players'] = 'Поиск Игроков';
$lang['search_alliances'] = 'Поиск Альянсов';

//Polls
$lang['vote'] = 'Голосование';
$lang['view_results'] = 'Результаты';
$lang['poll_results'] = 'Результаты голосований';
$lang['poll_vote_ok'] = 'Ваш голос был успешно засчитан';
$lang['poll_vote_error'] = 'В процессе обработки Вашего голоса возникли проблемы.';

//Colonize
$lang['og_colonize'] = 'Колонизировать';

//Flight times
$lang['time_calc'] = 'Калькулятор времени полета';
$lang['og_flight_times'] = 'Время полета';
$lang['start'] = 'начало';
$lang['destination'] = 'Назначение';

$lang['og_updating'] = 'Загрузка...';
$lang['og_wait_a_few_seconds'] = 'Подождите несколько секунд, а затем попробуйте<a href="%s%"> снова</a>';

$lang['flight_time_calculator'] = 'Калькулятор времени полета';
$lang['required_field'] = 'Это поле требует';
$lang['integer_n_digits'] = 'Только целые значения, дo %s% цифр'; 
$lang['integer_only'] = 'Без дробей!';
$lang['motors'] = 'Движки';
$lang['start_time'] = 'Время старта';
$lang['fleet_ships'] = 'Корабли';
$lang['uni_speed'] = 'Скорость вселенной';
$lang['fleet_speed'] = 'Скорость полета';
$lang['calc_times'] = 'Рассчитать!';

$lang['result_time'] = 'Общее время полета';
$lang['begin_hour'] = 'Время старта';
$lang['arriving_time'] = 'Время прибытия';
$lang['end_hour'] = 'Время окончания';

$lang['invalidDateTime'] = 'Введите время в правильном формате';
$lang['mustAddAShip'] = 'Укажи хоть кораблик!';
$lang['tools'] = 'Инструменты';

//Resources & Technologies
//Ogame API has already a translation for these. For example:
//http://uni101.ogame.fr/api/techNames.xml
$lang["small_cargo"] = 'Малый транспорт';
$lang["large_cargo"] = 'Большой транспорт';
$lang["light_fighter"] = 'Легкий истребитель';
$lang["heavy_fighter"] = 'Тяжелый истребитель';
$lang["cruiser"] = 'крейсер';
$lang["battle_ship"] = 'Линкор';
$lang["colony_ship"] = 'Колонизатор';
$lang["recycler"] = 'Переработчик';
$lang["esp_probe"] = 'Шпионский зонд';
$lang["bomber_ship"] = 'Бомбардировщик';
$lang["solar_sat"] = 'Солнечный спутник';
$lang["destroyer"] = 'Уничтожитель';
$lang["death_star"] = 'Звезда смерти';
$lang["battle_cruiser"] = 'Линейный крейсер';
$lang["rocket_launcher"] = 'Перехватчик';
$lang["light_laser"] = 'Легкий лазер';
$lang["heavy_laser"] = 'Тяжелый лазер';
$lang["gauss_cannon"] = 'Пушка Гаусса';
$lang["ion_cannon"] = 'Ионная пушка';
$lang["plasma_turret"] = 'Плазменное орудие';
$lang["small_shield_dome"] = 'Малый щитовой купол';
$lang["large_shield_dome"] = 'Большой щитовой купол';

$lang["military_tech"] = 'Оружейная технология';
$lang["defense_tech"] = 'Щитовая технология';
$lang["hull_tech"] = 'Броня космических кораблей';

$lang["combustion_drive_tech"] = 'Реактивный двигатель';
$lang["impulse_drive_tech"] = 'Импульсный двигатель';
$lang["hyperspace_drive_tech"] = 'Гиперпространственный двигатель ';
//

$lang['theme'] = 'Скин';
$lang['report_suggestions'] = 'Ваши пожелания и предложения шлите на <strong>tickets@ogniter.uservoice.com</strong>';

$lang['title_domain_module'] = 'Ogniter - Вселенные с: '; 
$lang['description_domain_module'] = 'Список вселенных: ';
$lang['description_fleet_simulator_module'] = 'Ogniter, Инструменты для Ogame. Полетный калькулятор';

$lang['title_server_index'] = 'Ogniter. Вселенная: %server%. Сервер:(%domain%)'; 
$lang['description_server_index'] = 'Ogame, Вселенная: %server% - Сервер:%domain% (Статистика)'; 

$lang['title_server_galaxy'] = 'Ogniter. %server%: Просмотр галактики [%location%] (%domain%)';
$lang['description_server_galaxy'] = 'Ogame, Вселенная: %server% (Обзор галактики) - %domain%';

$lang['title_server_search'] = 'Ogniter. %server%: Поиск игроков, планет и альянсов (%domain%)';
$lang['description_server_search'] = 'Ogame, Вселенная: %server% (Поисковая информация) - %domain%';

$lang['title_server_ranking'] = 'Ogniter. Вселенная %server%: Статистика (%domain%)';
$lang['description_server_ranking'] = 'Ogame, статистика по вселенной %server% - сервер %domain%';

$lang['title_server_alliance'] = 'Ogniter. %s% Альянс, %server% (%domain%)';
$lang['description_server_alliance'] = 'Ogame, Информация по статистике и игрокам %s% вселенной %server% - сервер %domain%';

$lang['title_server_player'] = 'Ogniter. %s% игрока, %server% (%domain%)';
$lang['description_server_player'] = 'Ogame, информация по статистике и галактике %s% вселенной %server% - сервер %domain%';

$lang['title_server_comparison'] = 'Ogniter. Сравните игроков или альянсов. %server% (%domain%)';
$lang['description_server_comparison'] = 'Ogame, сравнение игроков и Альянсов вселенной %server% - сервера %domain%';

$lang['title_server_statistics'] = 'Ogniter. Статистические графики, %server% (%domain%)';
$lang['description_server_statistics'] = 'Ogame, Статистика по вселенной %server% - сервер %domain%';

$lang['title_top_n_players'] = 'Ogniter. Топ %n% Игроков';
$lang['description_top_n_players'] = 'Ogniter. Статистика по топ %n% игрокам';

$lang['title_top_n_alliances'] = 'Ogniter. Топ %n% Альянсов';
$lang['description_top_n_alliances'] = 'Ogniter. Статистика по топ %n% альянсам';

$lang['pls_donate'] = 'Ogniter база данных со свободным доступом, размещенная на хостинге со средней стоимостью<br />
            Все Ваши пожертвования пойдут на оплату хостинга и разработку новых возможностей ogniter.org.<br /> Не забудте указать Ваше имя e-mail в форме связи!';

$lang['share'] = 'Поделиться';
$lang['planets'] = 'планеты';
$lang['moons'] = 'Луны';

$lang['discussions'] = 'Обсудить';

$lang['find_free_slots'] = 'Поиск свободных позиций';
$lang['occupied_planets'] = 'оккупированной планеты';
$lang['find_planets'] = 'Найдите планеты';
$lang['caption'] = 'описание';
$lang['free_slots_range'] = 'С %d по %d оккупированные планеты';
$lang['n_occupied_planets'] = '%d оккупированные планеты';

$lang['new'] = 'новое';
$lang['alliance_planets'] = 'Планеты альянс';
$lang['members_statistics'] = 'Пользователи по статистике';

$lang['description_server_track_alliance'] = 'Планета поиска альянса %s%, %server% - %domain%';
$lang['description_server_track_slots'] = 'Поиск попутных позиций в %server% - %domain%';
$lang['not_found_try_again'] = 'Не обнаружено. Пожалуйста, повторите попытку завтра.';

$lang['universe_limits'] = 'Пределы вселенной';
$lang['category'] = 'категория';
$lang['thread'] = 'тема';

$lang['galaxy_tools'] = 'Галактики инструменты';
$lang['planet_search_by_status'] = 'Поиск планет по статусу игроков';
$lang['player_status'] = "Игрок статус";
$lang['by_player_status'] = "Поиск по статусу игроков";
$lang['normal'] = 'нормальный';
$lang['popular_searches'] = 'поиск';

$lang['find_bandits_emperors'] = 'Find Bandits or Emperors';
$lang['grand_emperor'] = 'Grand Emperor';
$lang['emperor'] = 'Emperor';
$lang['star_lord'] = 'Star Lord';
$lang['bandit'] = 'Bandit';
$lang['bandit_lord'] = 'Bandit Lord';
$lang['bandit_king'] = 'Bandit King';

$lang['level'] = 'Level';
$lang['total_energy'] = 'Total energy';
$lang['max_temp'] = 'Max. temperature';
$lang['energy'] = 'Energy';
$lang['results'] = 'Results';
$lang['resources_needed'] = 'Resources needed';
$lang['calc_debris'] = 'Calculate resulting debris';

$lang['metal'] = 'Metal';
$lang['crystal'] = 'Crystal';
$lang['deuterium'] = 'Deuterium';

$lang['commander'] = 'Commander';
$lang['engineer'] = 'Engineer';
$lang['fleet_admiral'] = 'Fleet admiral';
$lang['geologist'] = 'Geologist';
$lang['technocrat'] = 'Technocrat';

$lang['pls_enable_javascript'] = 'You must have javascript enabled in order to use some parts of the site';

$lang['top_flop'] = 'Top &amp; Flop - %server% (%domain%)';
$lang['top_flop_description'] = 'Statistics. Top Increments and decrements on %server% (%domain%)';
$lang['tf_top_n_players'] = 'Top %n% increments - Players';
$lang['tf_top_n_alliances'] = 'Top %n% increments - Alliances';
$lang['tf_flop_n_players'] = 'Top %n% decrements - Players';
$lang['tf_flop_n_alliances'] = 'Top %n% decrements - Alliances';
$lang['top'] = 'Top';
$lang['flop'] = 'Flop';

$lang['by_day'] = 'By day';
$lang['faq_support'] = 'FAQ &amp; Support';
$lang['community_tools'] = 'Community tools';
